﻿
function changeDateFormat(inpdate) {
    var chDT = inpdate.split(' ')[0];
    var conDate = chDT.split('/')[1] + "/" + chDT.split('/')[0] + "/" + chDT.split('/')[2];
    var convDT = new Date(conDate + " " + inpdate.split(' ')[1]);
    var c_date = convDT.getFullYear() + "/" + (convDT.getMonth() + 1) + "/" + (convDT.getDate()) + " " + convDT.getHours() + ":" + convDT.getMinutes();
    return c_date;
}

function changeDateOnlyFormat(inpdate) {
    var chDT = inpdate;
    var conDate = chDT.split('/')[1] + "/" + chDT.split('/')[0] + "/" + chDT.split('/')[2];
    var convDT = new Date(conDate);
    var c_date = convDT.getFullYear() + "/" + (convDT.getMonth() + 1) + "/" + (convDT.getDate());
    return c_date;
}

function getregistrationList() {
    var aircraftregid = $("#LKAircraftTypeID").val();
    $.ajax
        ({
            url: '/Notifications/getregistrationList',
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            data: JSON.stringify({
                aircraftregid: +aircraftregid
            }),
            success: function (result) {
                $("#LKAircraftRegistrationID").html($('<option></option>').val(0).html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKAircraftRegistrationID").append
                        ($('<option></option>').val(item.LKAircraftRegistrationID).html(item.LKAircraftRegistrationName))
                });
            },
            error: function () {
                alert("Something went wrong..")
            },
        });
}

function getAirportNames(element_id) {
    $.ajax
        ({
            url: '/Notifications/getAirportList',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#LKDestinationAirportID_" + element_id).html();
                $("#LKDestinationAirportID_" + element_id).html($('<option></option>').val(0).html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKDestinationAirportID_" + element_id).append
                        ($('<option></option>').val(item.LKAirportID).html(item.LKAirportName))
                });

                $("#LKDepartureAirportID_" + element_id).html();
                $("#LKDepartureAirportID_" + element_id).html($('<option></option>').val(0).html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKDepartureAirportID_" + element_id).append
                        ($('<option></option>').val(item.LKAirportID).html(item.LKAirportName))
                });
            },
            error: function () {
                alert("Something went wrong..")
            },
        });
}

function getMakeList() {
    var aircraftmakeid = $("#LKAircraftMakeID").val();
    $.ajax
        ({
            url: '/Notifications/geMakeList',
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            data: JSON.stringify({
                aircraftmakeid: +aircraftmakeid
            }),
            success: function (result) {
                $("#LKAircraftModelID").html($('<option></option>').val('0').html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKAircraftModelID").append
                        ($('<option></option>').val(item.LKAircraftRegistrationID).html(item.LKAircraftRegistrationName))
                });
            },
            error: function () {
                alert("Something went wrong..")







            },
        });
}

function getMakeValues(element) {

    $.ajax
        ({
            url: '/Notifications/geMakeValues',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#LKAircraftModelID_" + element).html('');
                $("#LKAircraftModelID_" + element).html($('<option></option>').val('0').html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKAircraftModelID_" + element).append
                        ($('<option></option>').val(item.LKAircraftMakeID).html(item.LKAircraftMakeName))
                });
            },
            error: function () {
                alert("Something went wrong..")
            },
        });
}

function getCategoryValues(element) {

    $.ajax
        ({
            url: '/Notifications/geCategoryValues',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#AircraftCategoryId_" + element).html('');
                $("#AircraftCategoryId_" + element).html($('<option></option>').val(0).html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#AircraftCategoryId_" + element).append
                        ($('<option></option>').val(item.LKAircraftCategoryId).html(item.LKAircraftCategoryName))
                });
            },
            error: function () {
                alert("Something went wrong..")
            },
        });
}

function getregistrationValues(element) {
    $.ajax
        ({
            url: '/Notifications/getregistrationValues',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#LKAircraftRegistrationID_" + element).html('');
                $("#LKAircraftRegistrationID_" + element).html($('<option></option>').val(0).html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKAircraftRegistrationID_" + element).append
                        ($('<option></option>').val(item.LKAircraftRegistrationID).html(item.LKAircraftRegistrationName));

                    $("#addAircraftRegistration_" + element).html('Add Registration');

                    $("#LKAirlineOperatorID_" + element).html('');

                    $("#LKAircraftModelID_" + element).html('');

                    $("#AircraftCategoryId_" + element).html('');

                    $("#EngineInfo_" + element).val('');

                    $("#MaximumMass_" + element).val('');

                    $("#StateofRegistry_" + element).val('');

                    $("#stateofoperatorid_" + element).val('');

                    $("#OperatorContacts_" + element).val('');

                    $("#msn_" + element).val('');

                });
            },
            error: function () {
                alert("Something went wrong..")
            },
        });
}

function getOperatorNames(element) {
    $.ajax
        ({
            url: '/Notifications/getOperatorNames',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#LKAirlineOperatorID_" + element).html('');
                $("#LKAirlineOperatorID_" + element).html($('<option></option>').val(0).html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKAirlineOperatorID_" + element).append
                        ($('<option></option>').val(item.LKAirlineOperatorID).html(item.LKAirlineOperatorName))
                });
            },
            error: function () {
                alert("Something went wrong..")
            },
        });
}

function getOperatorNameByRegistrationId(e) {

    var ctrlId = e.id.split('_')[1];

    RegId = e.value;

    if (RegId == "") { RegId = parseInt(0); }

    $.ajax
        ({
            url: '/Notifications/getOperatorNameByRegistrationId',
            data: { RegId: parseInt(RegId) },
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#LKAirlineOperatorID_" + ctrlId).html('');
                if (RegId == 0) {
                    $("#addAircraftRegistration_" + ctrlId).html('Add Registration');
                    $("#LKAirlineOperatorID_" + ctrlId).append
                        ($('<option></option>').val(0).html(''));
                }
                else {

                    $("#addAircraftRegistration_" + ctrlId).html('Edit Registration');
                    $.each(JSON.parse(result), function (key, item) {
                        $("#LKAirlineOperatorID_" + ctrlId).append
                            ($('<option></option>').val(item.LKAirlineOperatorID).html(item.LKAirlineOperatorName));
                    });
                }
            },
            error: function () {
                $("#LKAirlineOperatorID_" + ctrlId).append
                    ($('<option></option>').val(0).html(''))
            },
        });
}

function getAircraftDetailsByRegistrationId(e) {

    var ctrlId = e.id.split('_')[1];

    RegId = e.value;

    if (RegId == "") { RegId = parseInt(0); }

    $.ajax
        ({
            url: '/Notifications/getAircraftDetailsByRegistrationId',
            data: { RegId: parseInt(RegId) },
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {

                $("#LKAirlineOperatorID_" + ctrlId).html('');
                $("#LKAircraftModelID_" + ctrlId).html('');
                $("#AircraftCategoryId_" + ctrlId).html('');

                if (RegId == 0) {

                    $("#addAircraftRegistration_" + ctrlId).html('Add Registration');

                    $("#LKAirlineOperatorID_" + ctrlId).append
                        ($('<option></option>').val(0).html(''))

                    $("#LKAircraftModelID_" + ctrlId).append
                        ($('<option></option>').val(0).html(''));

                    $("#AircraftCategoryId_" + ctrlId).append
                        ($('<option></option>').val(0).html(''));

                    $("#EngineInfo_" + ctrlId).val('');

                    $("#MaximumMass_" + ctrlId).val('');

                    $("#StateofRegistry_" + ctrlId).val('');

                    $("#stateofoperatorid_" + ctrlId).val('');

                    //$("#OperatorContacts_" + ctrlId).val('');

                    $("#msn_" + ctrlId).val('');
                }
                else {
                    $.each(JSON.parse(result), function (key, item) {

                        if ($("#LKAircraftRegistrationID_" + ctrlId).val() > 0) {

                            $("#addAircraftRegistration_" + ctrlId).html('Edit Registration');
                        }
                        else {
                            $("#addAircraftRegistration_" + ctrlId).html('Add Registration')
                        }

                        $("#LKAirlineOperatorID_" + ctrlId).append
                            ($('<option></option>').val(item.LKAirlineOperatorID).html(item.LKAirlineOperatorName));

                        $("#LKAircraftModelID_" + ctrlId).append
                            ($('<option></option>').val(item.LKAircraftModelID).html(item.LKAircraftModelName));

                        $("#AircraftCategoryId_" + ctrlId).append
                            ($('<option></option>').val(item.LKAircraftCategoryID).html(item.LKAircraftCategoryName));

                        $("#EngineInfo_" + ctrlId).val(item.EngineInfo);

                        $("#MaximumMass_" + ctrlId).val(item.MaximumMass);

                        $("#StateofRegistry_" + ctrlId).val(item.StateofRegistry);

                        $("#stateofoperatorid_" + ctrlId).val(item.StateofOperator);

                        //$("#OperatorContacts_" + ctrlId).val(item.OperatorContacts);

                        $("#msn_" + ctrlId).val(item.MSN);
                    });
                }
            },
            error: function () {
                
            },
        });
}

$(document).ready(function () {

    var deleteLinkObj;
   
    $('.delete-link').click(function () {
        deleteLinkObj = $(this);  
        $('#delete-dialog').dialog('open');
        return false; 
    });
    
    $('#delete-dialog').dialog({
        autoOpen: false, width: 400, resizable: false, modal: true, 
        buttons: {
            "Continue": function () {
                $.post(deleteLinkObj[0].href, function () {                      
                    window.location.href = '/Dashboard/DIDashboard';                   
                });
                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    $('.Srdelete-link').click(function () {
        deleteLinkObj = $(this);
        $('#srdelete-dialog').dialog('open');
        return false;
    });

    $('#srdelete-dialog').dialog({
        autoOpen: false, width: 400, resizable: false, modal: true,
        buttons: {
            "Continue": function () {
                $.post(deleteLinkObj[0].href, function () {
                    window.location.href = '/SR/SearchSR';
                });
                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    $('.Invdelete-link').click(function () {
        deleteLinkObj = $(this);
        $('#Invdelete-dialog').dialog('open');
        return false;
    });
   

    $('#Invdelete-dialog').dialog({
        autoOpen: false, width: 400, resizable: false, modal: true,
        buttons: {
            "Continue": function () {
                $.post(deleteLinkObj[0].href, function () {
                    window.location.href = '/Dashboard/InvestigatorDashboard';
                });
                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#add_aircrafts").click(function (e) {
        e.preventDefault();
        var aircraft_count = $("#NoOfAircraftInvolved").val();
        var acc_count = parseInt(aircraft_count) + parseInt(1);
        //var aircraft_info = '<div class="form-group">' +
        //    '<label class="col-sm-4 col-form-label" for="exampleInputEmail1"> Flight No<span class="text-danger"></span></label>' +
        //    '<div class="col-sm-8 col-form-box">' +
        //    '<input type="text" id="FlightNumber_' + acc_count + '" name="FlightNumber[]" class="form-control">' +
        //    '</div></div>'
        //    ;
        var aircraft_info = '<div class="add-notification-aircraft" id="acraft_details_id_' + acc_count + '">' +
            '<div class="info-tittle">' +
            '<h2>Aircraft ' + acc_count + ' <a style="float:right;" class="remove_aircraft" id="rmaircraft_' + acc_count + '" href="javascript:void(0);">Remove</a></h2>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Flight No<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="FlightNumber_' + acc_count + '" name="FlightNumber[]" class="form-control">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Destination Airport <a class="AirportReg" id="addAirport_' + acc_count + '" data-toggle="modal">Add/Edit Airport</a> <span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            //'<select class="form-control" id="LKDestinationAirportID_' + acc_count + '" name="LKDestinationAirportID[]">' + '</select>' +
            '<input type="text" id="DestinationAirportID_' + acc_count + '" name="DestinationAirportID[]" class="Airports form-control" placeholder="Start typing for suggestions">' +
            '<input type="hidden" name="LKDestinationAirportID[]" id="LKDestinationAirportID_' + acc_count + '" class="form-control" />' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Departure Airport <a class="AirportReg" id="addAirport_' + acc_count + '" data-toggle="modal">Add/Edit Airport</a> <span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            //'<select class="form-control" id="LKDepartureAirportID_' + acc_count + '" name="LKDepartureAirportID[]"></select>' +
            '<input type="text" id="DepartureAirportID_' + acc_count + '" name="DepartureAirportID[]" class="Airports form-control"  placeholder="Start typing for suggestions">' +
            '<input type="hidden" name="LKDepartureAirportID[]" id="LKDepartureAirportID_' + acc_count + '" class="form-control" />' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Long & Lat<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="LongLat_' + acc_count + '" name="LongLat[]" class="form-control">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Passengers Onboard<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="PassengerOnboard_' + acc_count + '" name="PassengerOnboard[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4" min = "0" s max = "1000">' +
            '</div> </div></div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Cockpit Crew Onboard<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="CrewOnboard_' + acc_count + '" name="CrewOnboard[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4"  min = "0" max = "1000">' +
            '</div> </div> '+
            '  </div > </div ></div > ' +

            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Cabin Crew Onboard<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="CabinCrew_' + acc_count + '" name="CabinCrew[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4"  min = "0" max = "1000">' +
            '</div> </div> ' +
            '  </div > </div ></div > ' +

            '<div class="col-md-12">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Injuries to Persons<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<div class="form-min-table">' +
            '<table class="table table-bordered">' +
            '<thead>' +
            '<tr>' +
            '<th>Injuries </th>' +
            '<th>Crew<span class="text-danger"></span></th>' +
            '<th>Passengers<span class="text-danger"></span></th>' +
            '<th>Others<span class="text-danger"></span></th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>' +
            '<tr>' +
            '<td>Fatal</td>' +
            '<td><input type="number" id="CrewsWithFatalInjury_' + acc_count + '" name="CrewsWithFatalInjury[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="PassengersWithFatalInjury_' + acc_count + '" name="PassengersWithFatalInjury[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="OthersWithFatalInjury_' + acc_count + '" name="OthersWithFatalInjury[]" value="0" class="form-control onboard-not"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>Serious</td>' +
            '<td><input type="number" id="CrewsWithSeriousInjury_' + acc_count + '" name="CrewsWithSeriousInjury[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="PassengersWithSeriousInjury_' + acc_count + '" name="PassengersWithSeriousInjury[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="OthersWithSeriousInjury_' + acc_count + '" name="OthersWithSeriousInjury[]" value="0" class="form-control onboard-not"></td>' +
            ' </tr>' +
            '<tr>' +
            '<td>Minor</td>' +
            '<td><input type="number" id="CrewsWithMinorInjury_' + acc_count + '" name="CrewsWithMinorInjury[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="PassengersWithMinorInjury_' + acc_count + '" name="PassengersWithMinorInjury[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="OthersWithMinorInjury_' + acc_count + '" name="OthersWithMinorInjury[]" value="0" class="form-control onboard-not"></td>' +
            '</tr>' +
            '<tr>' +
            '<td>None</td>' +
            '<td><input type="number" id="CrewsKilled_' + acc_count + '" name="CrewsKilled[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="PassengersKilled_' + acc_count + '" name="PassengersKilled[]" value="0" class="form-control onboard-not"></td>' +
            '<td><input type="number" id="OthersKilled_' + acc_count + '" name="OthersKilled[]" value="0" class="form-control onboard-not"></td>' +

            '</tr>' +
            '</tbody>' +
            '</table>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Registration <a class="AircraftReg" id="addAircraftRegistration_' + acc_count + '" data-toggle="modal"> Add Registration</a></label>' +
            ' <div class="col-sm-12 col-form-box">' +
            '<select class="form-control" id="LKAircraftRegistrationID_' + acc_count + '" name="LKAircraftRegistrationID[]" onchange = "getAircraftDetailsByRegistrationId(this)" ></select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Make & Model<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<select class="customDrowDown regist-disa form-control" id="LKAircraftModelID_' + acc_count + '" name="LKAircraftModelID[]" disabled="disabled"></select>' +
            '</div>' +
            ' </div>' +
            ' </div>' +         
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Engine Info<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="EngineInfo_' + acc_count + '" name="EngineInfo[]" class="regist-disa form-control" readonly="readonly">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Maximum Take Off Mass (kg)<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="MaximumMass_' + acc_count + '" name="MaximumMass[]" class="regist-disa form-control" readonly="readonly">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Aircraft Category<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<select class="customDrowDown regist-disa form-control" id="AircraftCategoryId_' + acc_count + '" name="AircraftCategoryId[]" disabled="disabled"></select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'State of Registry<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="StateofRegistry_' + acc_count + '" name="StateofRegistry[]" class="regist-disa form-control" readonly="readonly">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            ' </div>'+
            '</div>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Operator Name<a class="airOperName" id="addOperatorName_' + acc_count + '" data-toggle="modal"></a></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<select class="customDrowDown regist-disa form-control" id="LKAirlineOperatorID_' + acc_count + '" name="LKAirlineOperatorID[]" disabled="disabled"></select>' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'State of the Operator' +
            '<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="stateofoperatorid_' + acc_count + '" name="stateofoperatorid[]" class="regist-disa form-control" readonly="readonly">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'Operator Contacts' +
            '<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input onkeydown = "return ValidateNumber(event);"  maxlength = "10" id="OperatorContacts_' + acc_count + '" name="OperatorContacts[]" class="form-control">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'MSN<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="msn_' + acc_count + '" name="msn[]" class="regist-disa form-control" readonly="readonly" >'
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            $("#aircrafts_div").append(aircraft_info);
            $("#NoOfAircraftInvolved").val(acc_count);
            $("#aircrafts_inv_no").html(acc_count);
        getAirportNames(acc_count);
        //getMakeValues(acc_count);
        //getCategoryValues(acc_count);
        getregistrationValues(acc_count);
        //getOperatorNames(acc_count); //on 12-june-2018
    });

    $("#addnotification_aircrafts").click(function (e) {
        e.preventDefault();
        var aircraft_count = $("#NoOfAircraftInvolved").val();
        var acc_count = parseInt(aircraft_count) + parseInt(1);
        //var aircraft_info = '<div class="form-group">' +
        //    '<label class="col-sm-4 col-form-label" for="exampleInputEmail1"> Flight No<span class="text-danger"></span></label>' +
        //    '<div class="col-sm-8 col-form-box">' +
        //    '<input type="text" id="FlightNumber_' + acc_count + '" name="FlightNumber[]" class="form-control">' +
        //    '</div></div>'
        //    ;
        var aircraft_info = '<div class="add-notification-aircraft" id="acraft_details_id_' + acc_count + '">' +
            '<div class="info-tittle">' +
            '<h2>Aircraft ' + acc_count + ' <a style="float:right;" class="remove_aircraft" id="rmaircraft_' + acc_count + '" href="javascript:void(0);">Remove</a></h2>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Flight No<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="FlightNumber_' + acc_count + '" name="FlightNumber[]" class="form-control">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Destination Airport <a class="AirportReg" id="addAirport_' + acc_count + '" data-toggle="modal">Add/Edit Airport</a><span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            //'<select class="form-control" id="LKDestinationAirportID_' + acc_count + '" name="LKDestinationAirportID[]">' + '</select>' +
            '<input type="text" id="DestinationAirportID_' + acc_count + '" name="DestinationAirportID[]" class="Airports form-control" placeholder="Start typing for suggestions">' +
            '<input type="hidden" name="LKDestinationAirportID[]" id="LKDestinationAirportID_' + acc_count + '" class="form-control" />' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Departure Airport <a class="AirportReg" id="addAirport_' + acc_count + '" data-toggle="modal">Add/Edit Airport</a><span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            //'<select class="form-control" id="LKDepartureAirportID_' + acc_count + '" name="LKDepartureAirportID[]"></select>' +
            '<input type="text" id="DepartureAirportID_' + acc_count + '" name="DepartureAirportID[]" class="Airports form-control"  placeholder="Start typing for suggestions">' +
            '<input type="hidden" name="LKDepartureAirportID[]" id="LKDepartureAirportID_' + acc_count + '" class="form-control" />' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +


            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Diverted To Airport <a class="AirportReg" id="addAirport_' + acc_count + '" data-toggle="modal">Add/Edit Airport</a><span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +            
            '<input type="text" id="DivertedToAirportID_' + acc_count + '" name="DivertedToAirportID[]" class="Airports form-control"  placeholder="Start typing for suggestions">' +
            '<input type="hidden" name="LKDivertedToAirportID[]" id="LKDivertedToAirportID_' + acc_count + '" class="form-control" />' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Passengers Injured<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="TotalPassengersInjuries_' + acc_count + '" name="TotalPassengersInjuries[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4" min = "0" s max = "1000">' +
            '</div> </div></div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Crew Injured<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="TotalCrewsInjuries_' + acc_count + '" name="TotalCrewsInjuries[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4"  min = "0" max = "1000">' +
            '</div> </div>' +
            ' </div > </div ></div > ' +


            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Long & Lat<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="LongLat_' + acc_count + '" name="LongLat[]" class="form-control">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Passengers Onboard<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="PassengerOnboard_' + acc_count + '" name="PassengerOnboard[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4" min = "0" s max = "1000">' +
            '</div> </div></div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Cockpit Crew Onboard<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="CrewOnboard_' + acc_count + '" name="CrewOnboard[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4"  min = "0" max = "1000">' +
            '</div> </div>' +
            ' </div > </div ></div > ' +

            '<div class="col-md-12">' +
            '<div class="row">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Cabin Crew Onboard<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input id="CabinCrew_' + acc_count + '" name="CabinCrew[]" class="form-control onboard-not" type = "text" value="0" onkeydown = "return ValidateNumber(event);" maxlength = "4"  min = "0" max = "1000">' +
            '</div> </div> ' +
            ' </div >' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Registration <a class="AircraftReg" id="addAircraftRegistration_' + acc_count + '" data-toggle="modal"> Add Registration</a></label>' +
            ' <div class="col-sm-12 col-form-box">' +
            '<select class="form-control" id="LKAircraftRegistrationID_' + acc_count + '" name="LKAircraftRegistrationID[]" onchange = "getAircraftDetailsByRegistrationId(this)" ></select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Make & Model<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<select class="customDrowDown regist-disa form-control" id="LKAircraftModelID_' + acc_count + '" name="LKAircraftModelID[]" disabled="disabled"></select>' +
            '</div>' +
            ' </div>' +
            ' </div>' +
        '</div ></div > ' +

           // '<div class="col-md-12">' +
            //'<div class="form-group">' +
            //'<label class="col-sm-12 col-form-label" for="exampleInputPassword1">Injuries to Persons<span class="text-danger"></span></label>' +
            //'<div class="col-sm-12 col-form-box">' +
            //'<div class="form-min-table">' +
            //'<table class="table table-bordered">' +
            //'<thead>' +
            //'<tr>' +
            //'<th>Injuries </th>' +
            //'<th>Crew<span class="text-danger"></span></th>' +
            //'<th>Passengers<span class="text-danger"></span></th>' +
            //'<th>Others<span class="text-danger"></span></th>' +
            //'</tr>' +
            //'</thead>' +
            //'<tbody>' +
            //'<tr>' +
            //'<td>Fatal</td>' +
            //'<td><input type="number" id="CrewsWithFatalInjury_' + acc_count + '" name="CrewsWithFatalInjury[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="PassengersWithFatalInjury_' + acc_count + '" name="PassengersWithFatalInjury[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="OthersWithFatalInjury_' + acc_count + '" name="OthersWithFatalInjury[]" value="0" class="form-control"></td>' +
            //'</tr>' +
            //'<tr>' +
            //'<td>Serious</td>' +
            //'<td><input type="number" id="CrewsWithSeriousInjury_' + acc_count + '" name="CrewsWithSeriousInjury[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="PassengersWithSeriousInjury_' + acc_count + '" name="PassengersWithSeriousInjury[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="OthersWithSeriousInjury_' + acc_count + '" name="OthersWithSeriousInjury[]" value="0" class="form-control"></td>' +
            //' </tr>' +
            //'<tr>' +
            //'<td>Minor</td>' +
            //'<td><input type="number" id="CrewsWithMinorInjury_' + acc_count + '" name="CrewsWithMinorInjury[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="PassengersWithMinorInjury_' + acc_count + '" name="PassengersWithMinorInjury[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="OthersWithMinorInjury_' + acc_count + '" name="OthersWithMinorInjury[]" value="0" class="form-control"></td>' +
            //'</tr>' +
            //'<tr>' +
            //'<td>None</td>' +
            //'<td><input type="number" id="CrewsKilled_' + acc_count + '" name="CrewsKilled[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="PassengersKilled_' + acc_count + '" name="PassengersKilled[]" value="0" class="form-control"></td>' +
            //'<td><input type="number" id="OthersKilled_' + acc_count + '" name="OthersKilled[]" value="0" class="form-control"></td>' +

            //'</tr>' +
            //'</tbody>' +
            //'</table>' +
            //'</div>' +
            //'</div>' +
            //'</div>' +
            //'</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
            
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Engine Info<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="EngineInfo_' + acc_count + '" name="EngineInfo[]" class="regist-disa form-control" readonly="readonly">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Maximum Take Off Mass (kg)<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="MaximumMass_' + acc_count + '" name="MaximumMass[]" class="regist-disa form-control" readonly="readonly">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Aircraft Category<span class="text-danger"></span></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<select class="customDrowDown regist-disa form-control" id="AircraftCategoryId_' + acc_count + '" name="AircraftCategoryId[]" disabled="disabled"></select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +
        
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'State of Registry<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="StateofRegistry_' + acc_count + '" name="StateofRegistry[]" class="regist-disa form-control" readonly="readonly">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            ' </div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">Operator Name<a class="airOperName" id="addOperatorName_' + acc_count + '" data-toggle="modal"></a></label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<select class="customDrowDown regist-disa form-control" id="LKAirlineOperatorID_' + acc_count + '" name="LKAirlineOperatorID[]" disabled="disabled"></select>' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'State of the Operator' +
            '<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="stateofoperatorid_' + acc_count + '" name="stateofoperatorid[]" class="regist-disa form-control" readonly="readonly">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-12">' +
            '<div class="row">' +            

            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'Operator Contacts' +
            '<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input onkeydown = "return ValidateNumber(event);"  maxlength = "10" id="OperatorContacts_' + acc_count + '" name="OperatorContacts[]" class="form-control">' +
            '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label class="col-sm-12 col-form-label" for="exampleInputEmail1">' +
            'MSN<span class="text-danger"></span>' +
            '</label>' +
            '<div class="col-sm-12 col-form-box">' +
            '<input type="text" id="msn_' + acc_count + '" name="msn[]" class="regist-disa form-control" readonly="readonly" >'
        '<small id="emailHelp" class="form-text text-muted"></small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            $("#aircrafts_div").append(aircraft_info);
        $("#NoOfAircraftInvolved").val(acc_count);
        $("#aircrafts_inv_no").html(acc_count);
        getAirportNames(acc_count);
        //getMakeValues(acc_count);
        //getCategoryValues(acc_count);
        getregistrationValues(acc_count);
        //getOperatorNames(acc_count); //on 12-june-2018
    });

    $("#Save_extdetails").on("submit", function (e) {

        e.preventDefault();
        var first_name = [];
        first_name = $("input[name='FirstName[]'")
            .map(function () { return $(this).val(); }).get();
        var last_name = [];
        last_name = $("input[name='LastName[]'")
            .map(function () { return $(this).val(); }).get();
        var email = [];
        email_ = $("input[name='Email[]'")
            .map(function () { return $(this).val(); }).get();
        var operator_name = [];
        operator_name = $("input[name='holdername[]'")
            .map(function () { return $(this).val(); }).get();

        e.preventDefault();

        var Ext = {

            "FirstName_": first_name,
            "LastName_": last_name,
            "Email_id": email_,
            "OperatorName": operator_name

        };
        console.log(Ext);
        $.ajax({

            url: "/task/ExternalHolder_Details",
            type: "Post",
            data: JSON.stringify(Ext),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {

                alert("successful");

                //alert("successful");
                //  $('#notifyModal').modal('show');

            },
            error: function (msg) { alert(msg); }
        });
    });

    $("#new_occurrence").on("submit", function (e) {

        e.preventDefault();
        var flight_no = [];
        flight_no = $("input[name='FlightNumber[]'")
            .map(function () { return $(this).val(); }).get();
        var Dest_airport = [];
        //Dest_airport = $("select[name='LKDestinationAirportID[]'")
        //    .map(function () {
        //        if ($(this).val() == '')
        //            return 0;
        //        else
        //            return $(this).val();
        //    }).get();
        Dest_airport = $("input[name='LKDestinationAirportID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Dept_airport = [];
        //Dept_airport = $("select[name='LKDepartureAirportID[]'")
        //    .map(function () {
        //        if ($(this).val() == '')
        //            return 0;
        //        else
        //            return $(this).val();
        //    }).get();
        Dept_airport = $("input[name='LKDepartureAirportID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var long_lat = [];
        long_lat = $("input[name='LongLat[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Passenger_Onboard = [];
        Passenger_Onboard = $("input[name='PassengerOnboard[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var crew_Onboard = [];
        crew_Onboard = $("input[name='CrewOnboard[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var Crews_WithFatal_Injury = [];
        Crews_WithFatal_Injury = $("input[name='CrewsWithFatalInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var Passengers_With_FatalInjury = [];
        Passengers_With_FatalInjury = $("input[name='PassengersWithFatalInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var Others_With_FatalInjury = [];
        Others_With_FatalInjury = $("input[name='OthersWithFatalInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Crews_With_SeriousInjury = [];
        Crews_With_SeriousInjury = $("input[name='CrewsWithSeriousInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Passengers_With_SeriousInjury = [];
        Passengers_With_SeriousInjury = $("input[name='PassengersWithSeriousInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Others_With_SeriousInjury = [];
        Others_With_SeriousInjury = $("input[name='OthersWithSeriousInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Crews_With_MinorInjury = [];
        Crews_With_MinorInjury = $("input[name='CrewsWithMinorInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Passengers_With_MinorInjury = [];
        Passengers_With_MinorInjury = $("input[name='PassengersWithMinorInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Others_With_MinorInjury = [];
        Others_With_MinorInjury = $("input[name='OthersWithMinorInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Crews_Killed = [];
        Crews_Killed = $("input[name='CrewsKilled[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Passengers_Killed = [];
        Passengers_Killed = $("input[name='PassengersKilled[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Others_Killed = [];
        Others_Killed = $("input[name='OthersKilled[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var LKAircraft_ModelID = [];
        LKAircraft_ModelID = $("select[name='LKAircraftModelID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var LKAircraft_RegistrationID = [];
        LKAircraft_RegistrationID = $("select[name='LKAircraftRegistrationID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Engine_Info = [];
        Engine_Info = $("input[name='EngineInfo[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Max_Mass = [];
        Max_Mass = $("input[name='MaximumMass[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Aircraft_CategoryId = [];
        Aircraft_CategoryId = $("select[name='AircraftCategoryId[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Stateof_Registry = [];
        Stateof_Registry = $("input[name='StateofRegistry[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var LKAirline_OperatorID = [];
        LKAirline_OperatorID = $("select[name='LKAirlineOperatorID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var state_ofoperatorid = [];
        state_ofoperatorid = $("input[name='stateofoperatorid[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Operator_Contacts = [];
        Operator_Contacts = $("input[name='OperatorContacts[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var msn_ = [];
        msn_ = $("input[name='msn[]'")
            .map(function () {
                return $(this).val();
            }).get();

        var CabinCrew_ = [];
        CabinCrew_ = $("input[name='CabinCrew[]'")
            .map(function () {
                return $(this).val();
            }).get();


        var Divert_airport = [];        
        Divert_airport = $("input[name='LKDivertedToAirportID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var CrewsInjuries_ = [];
        CrewsInjuries_ = $("input[name='TotalCrewsInjuries[]'")
            .map(function () {
                return $(this).val();
            }).get();

        var PassengersInjuries_ = [];
        PassengersInjuries_ = $("input[name='TotalPassengersInjuries[]'")
            .map(function () {
                return $(this).val();
            }).get();

        e.preventDefault();
        var dte = changeDateFormat($("#LocalDateAndTimeOfOccurrence").val());
       
        var person = {
            "TotalCrewsInjuries": CrewsInjuries_,
            "TotalPassengersInjuries": PassengersInjuries_,
            "LKDivertedToAirportID": Divert_airport,
            "UpdatedInformation": $("#UpdatedInformation").val(),
            "OccurrenceCategorizationId": $("#OccurrenceCategorizationId").val(),
            "CabinCrew": CabinCrew_,
            "OccurrenceDetailID": $("#OccurrenceDetailID").val(),
            "FlightNumber": flight_no,
            "LKDestinationAirportID": Dest_airport,
            "LKDepartureAirportID": Dept_airport,
            "LongLat": long_lat,
            "PassengerOnboard": Passenger_Onboard,
            "CrewOnboard": crew_Onboard,
            "CrewsWithFatalInjury": Crews_WithFatal_Injury,
            "PassengersWithFatalInjury": Passengers_With_FatalInjury,
            "OthersWithFatalInjury": Others_With_FatalInjury,
            "CrewsWithSeriousInjury": Crews_With_SeriousInjury,
            "PassengersWithSeriousInjury": Passengers_With_SeriousInjury,
            "OthersWithSeriousInjury": Others_With_SeriousInjury,
            "CrewsWithMinorInjury": Crews_With_MinorInjury,
            "PassengersWithMinorInjury": Passengers_With_MinorInjury,
            "OthersWithMinorInjury": Others_With_MinorInjury,
            "CrewsKilled": Crews_Killed,
            "PassengersKilled": Passengers_Killed,
            "OthersKilled": Others_Killed,
            "LKAircraftModelID": LKAircraft_ModelID,
            "LKAircraftRegistrationID": LKAircraft_RegistrationID,
            "EngineInfo": Engine_Info,
            "MaximumMass": Max_Mass,
            "LKAircraftcategoryid": Aircraft_CategoryId,
            "StateofRegistry": Stateof_Registry,
            "LKAirlineOperatorID": LKAirline_OperatorID,
            "stateofoperatorid": state_ofoperatorid,
            "OperatorContacts": Operator_Contacts,
            "msn": msn_,
            "NoofAircraft": $("#NoOfAircraftInvolved").val(),
            //"LKAircraftMakeID": $("#LKAircraftMakeID").val(),
            //"LKAircraftModelID": $("#LKAircraftModelID").val(),
            //"LKAircraftTypeID": $("#LKAircraftTypeID").val(),
            //"FlightNumber": $("#FlightNumber").val(),
            "LKCountryID": $("#LKCountryID").val(),
            //"LKAircraftRegistrationID": $("#LKAircraftRegistrationID").val(),
            //"NameOfRegisteredOwner": $("#NameOfRegisteredOwner").val(),
            //"LKAirlineOperatorID": $("#LKAirlineOperatorID").val(),
            //"QualificationOfPilot": $("#QualificationOfPilot").val(),
            //"PilotCertificateNumber": $("#PilotCertificateNumber").val(),
            ////"LocalDateAndTimeOfOccurrence": $("#LocalDateAndTimeOfOccurrence").val(),
            "LocalDateAndTimeOfOccurrence": changeDateFormat($("#LocalDateAndTimeOfOccurrence").val()),
            "OccurrenceDateTime": changeDateFormat($("#OccurrenceDateTime").val()),
            //"LKDepartureAirportID": $("#LKDepartureAirportID").val(),
            //"LKDestinationAirportID": $("#LKDestinationAirportID").val(),
            //"PersonsAboard": $("#PersonsAboard").val(),
            //"PersonsSeriouslyInjured": $("#PersonsSeriouslyInjured").val(),
            //"PersonsKilled": $("#PersonsKilled").val(),
            //"PersonsWithMinorInjury": $("#PersonsWithMinorInjury").val(),
            "LKIncidentTypeID": $("#LKIncidentTypeID").val(),
            "PrevailingWeatherConditions": $("#PrevailingWeatherConditions").val(),
            "ExtendOfDamageToAircraft": $("#ExtendOfDamageToAircraft").val(),
            "DamagesToGroundObjects": $("#DamagesToGroundObjects").val(),
            "DescriptionOfExplosivesAndDangerousArticles": $("#DescriptionOfExplosivesAndDangerousArticles").val(),
            "LKOccurrenceInformerID": $("#LKOccurrenceInformerID").val(),
            "Notifier": $("#Notifier").val(),
            "PlaceofIncident": $("#PlaceofIncident").val(),
            "Notifier_Contact": $("#Notifier_Contact").val(),
            "DIsecurityNotify": $("#DIsecurityNotify:checked").val(),
            "DatetimeofCall": changeDateFormat($("#NotifierCallDate").val()),
            "DescriptionOfOccurrence": $("#DescriptionOfOccurrence").val(),
            "LKOccurrenceStatusTypeId": 2,
            "NotificationDescription": $("#NotificationDescription").val(),
            "NotificationId": $("#NotificationId").val()
        };
        console.log(person);
        $.ajax({

            url: "/Notifications/OccurenceNotify",
            type: "Post",
            data: JSON.stringify(person),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {               
                if (data === 0) {
                    window.location.href = '/home/Dashboard';
                }
                //else if (data === "Format") {
                //    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i> Invalid Notification File No Format </div>');
                //    $('.alert-danger').show();
                //    setTimeout(function () { $(".alert").hide(); }, 2000);
                //}
                //else if (data === "Exist") {
                //    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i> Notification File No Already Exist </div>');
                //    $('.alert-danger').show();
                //    setTimeout(function () { $(".alert").hide(); }, 2000);
                //}
                else  {
                    sendNotificationDL(data);
                }
            },
            error: function (msg) { alert(msg); }
        });
    });

    $(document.body).on('click', '.remove_aircraft', function () {
        var aircraft_id = $(this).attr('id').split('_');
        if (aircraft_id[1]) {
            var acid = aircraft_id[1]
            $("#acraft_details_id_" + acid).remove();
            var old_count = $("#NoOfAircraftInvolved").val();
            var new_count = parseInt(old_count) - parseInt(1);
            $("#NoOfAircraftInvolved").val(new_count);
            $("#aircrafts_inv_no").html(new_count);
        }
    });

    $(document.body).on('click', '.remove_added_aircraft', function () {
        var aircraft_id = $(this).attr('id').split('_');
        if (aircraft_id[2]) {
            var acid = parseInt(aircraft_id[2]);
            $.ajax({
                type: 'POST',
                cache: false,
                url: '/Notifications/RemoveNotificationAircraft',
                datatype: 'json',
                data: { air_id: parseInt(acid) },
                success: function (data) {
                    if (data == 'success') {
                        $('.alert-success').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Aircrat Details Removed Successfully</div>');
                        $('.alert-success').show();
                        var old_count = $("#NoOfAircraftInvolved").val();
                        var new_count = parseInt(old_count) - parseInt(1);
                        $("#NoOfAircraftInvolved").val(new_count);
                        $("#aircrafts_inv_no").html(new_count);
                        setTimeout(function () { window.location.reload(); }, 2000);
                    } else {
                        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Unable to remove Aircrat Details!!</div>');
                        $('.alert-danger').show();
                        setTimeout(function () { window.location.reload(); }, 2000);
                    }
                },
                error: function (msg) { alert(msg); }
            });
        }


    });

    $("#button_save").click(function (e) {
        var flight_no = [];
        flight_no = $("input[name='FlightNumber[]'")
            .map(function () { return $(this).val(); }).get();
        var Dest_airport = [];
        //Dest_airport = $("select[name='LKDestinationAirportID[]'")
        //    .map(function () {
        //        if ($(this).val() == '')
        //            return 0;
        //        else
        //            return $(this).val();
        //    }).get();
        Dest_airport = $("input[name='LKDestinationAirportID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Dept_airport = [];
        //Dept_airport = $("select[name='LKDepartureAirportID[]'")
        //    .map(function () {
        //        if ($(this).val() == '')
        //            return 0;
        //        else
        //            return $(this).val();
        //    }).get();
        Dept_airport = $("input[name='LKDepartureAirportID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var long_lat = [];
        long_lat = $("input[name='LongLat[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Passenger_Onboard = [];
        Passenger_Onboard = $("input[name='PassengerOnboard[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var crew_Onboard = [];
        crew_Onboard = $("input[name='CrewOnboard[]'")


            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var Crews_WithFatal_Injury = [];
        Crews_WithFatal_Injury = $("input[name='CrewsWithFatalInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var Passengers_With_FatalInjury = [];
        Passengers_With_FatalInjury = $("input[name='PassengersWithFatalInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var Others_With_FatalInjury = [];
        Others_With_FatalInjury = $("input[name='OthersWithFatalInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Crews_With_SeriousInjury = [];
        Crews_With_SeriousInjury = $("input[name='CrewsWithSeriousInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Passengers_With_SeriousInjury = [];
        Passengers_With_SeriousInjury = $("input[name='PassengersWithSeriousInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Others_With_SeriousInjury = [];
        Others_With_SeriousInjury = $("input[name='OthersWithSeriousInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Crews_With_MinorInjury = [];
        Crews_With_MinorInjury = $("input[name='CrewsWithMinorInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Passengers_With_MinorInjury = [];
        Passengers_With_MinorInjury = $("input[name='PassengersWithMinorInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Others_With_MinorInjury = [];
        Others_With_MinorInjury = $("input[name='OthersWithMinorInjury[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Crews_Killed = [];
        Crews_Killed = $("input[name='CrewsKilled[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Passengers_Killed = [];
        Passengers_Killed = $("input[name='PassengersKilled[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Others_Killed = [];
        Others_Killed = $("input[name='OthersKilled[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var LKAircraft_ModelID = [];
        LKAircraft_ModelID = $("select[name='LKAircraftModelID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var LKAircraft_RegistrationID = [];
        LKAircraft_RegistrationID = $("select[name='LKAircraftRegistrationID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Engine_Info = [];
        Engine_Info = $("input[name='EngineInfo[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Max_Mass = [];
        Max_Mass = $("input[name='MaximumMass[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Aircraft_CategoryId = [];
        Aircraft_CategoryId = $("select[name='AircraftCategoryId[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Stateof_Registry = [];
        Stateof_Registry = $("input[name='StateofRegistry[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var LKAirline_OperatorID = [];
        LKAirline_OperatorID = $("select[name='LKAirlineOperatorID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var state_ofoperatorid = [];
        state_ofoperatorid = $("input[name='stateofoperatorid[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var Operator_Contacts = [];
        Operator_Contacts = $("input[name='OperatorContacts[]'")
            .map(function () {
                return $(this).val();
            }).get();
        var msn_ = [];
        msn_ = $("input[name='msn[]'")
            .map(function () {
                return $(this).val();
            }).get();

        var CabinCrew_ = [];
        CabinCrew_ = $("input[name='CabinCrew[]'")
            .map(function () {
                return $(this).val();
            }).get();

        var Divert_airport = [];
        Divert_airport = $("input[name='LKDivertedToAirportID[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();

        var CrewsInjuries_ = [];
        CrewsInjuries_ = $("input[name='TotalCrewsInjuries[]'")
            .map(function () {
                return $(this).val();
            }).get();

        var PassengersInjuries_ = [];
        PassengersInjuries_ = $("input[name='TotalPassengersInjuries[]'")
            .map(function () {
                return $(this).val();
            }).get();

        e.preventDefault();
        var dte = changeDateFormat($("#LocalDateAndTimeOfOccurrence").val());
      
        var person = {

            "TotalCrewsInjuries": CrewsInjuries_,
            "TotalPassengersInjuries": PassengersInjuries_,
            "LKDivertedToAirportID": Divert_airport,
            "UpdatedInformation": $("#UpdatedInformation").val(),
            "OccurrenceCategorizationId": $("#OccurrenceCategorizationId").val(),
            "CabinCrew": CabinCrew_,
            "OccurrenceDetailID": $("#OccurrenceDetailID").val(),
            "FlightNumber": flight_no,
            "LKDestinationAirportID": Dest_airport,
            "LKDepartureAirportID": Dept_airport,
            "LongLat": long_lat,
            "PassengerOnboard": Passenger_Onboard,
            "CrewOnboard": crew_Onboard,
            "CrewsWithFatalInjury": Crews_WithFatal_Injury,
            "PassengersWithFatalInjury": Passengers_With_FatalInjury,
            "OthersWithFatalInjury": Others_With_FatalInjury,
            "CrewsWithSeriousInjury": Crews_With_SeriousInjury,
            "PassengersWithSeriousInjury": Passengers_With_SeriousInjury,
            "OthersWithSeriousInjury": Others_With_SeriousInjury,
            "CrewsWithMinorInjury": Crews_With_MinorInjury,
            "PassengersWithMinorInjury": Passengers_With_MinorInjury,
            "OthersWithMinorInjury": Others_With_MinorInjury,
            "CrewsKilled": Crews_Killed,
            "PassengersKilled": Passengers_Killed,
            "OthersKilled": Others_Killed,
            "LKAircraftModelID": LKAircraft_ModelID,
            "LKAircraftRegistrationID": LKAircraft_RegistrationID,
            "EngineInfo": Engine_Info,
            "MaximumMass": Max_Mass,
            "LKAircraftcategoryid": Aircraft_CategoryId,
            "StateofRegistry": Stateof_Registry,
            "LKAirlineOperatorID": LKAirline_OperatorID,
            "stateofoperatorid": state_ofoperatorid,
            "OperatorContacts": Operator_Contacts,
            "msn": msn_,
            "NoofAircraft": $("#NoOfAircraftInvolved").val(),
            //"LKAircraftMakeID": $("#LKAircraftMakeID").val(),
            //"LKAircraftModelID": $("#LKAircraftModelID").val(),
            //"LKAircraftTypeID": $("#LKAircraftTypeID").val(),
            //"FlightNumber": $("#FlightNumber").val(),
            "LKCountryID": $("#LKCountryID").val(),
            //"LKAircraftRegistrationID": $("#LKAircraftRegistrationID").val(),
            //"NameOfRegisteredOwner": $("#NameOfRegisteredOwner").val(),
            //"LKAirlineOperatorID": $("#LKAirlineOperatorID").val(),
            //"QualificationOfPilot": $("#QualificationOfPilot").val(),
            //"PilotCertificateNumber": $("#PilotCertificateNumber").val(),
            ////"LocalDateAndTimeOfOccurrence": $("#LocalDateAndTimeOfOccurrence").val(),
            "LocalDateAndTimeOfOccurrence": changeDateFormat($("#LocalDateAndTimeOfOccurrence").val()),
            "OccurrenceDateTime": changeDateFormat($("#OccurrenceDateTime").val()),
            //"LKDepartureAirportID": $("#LKDepartureAirportID").val(),
            //"LKDestinationAirportID": $("#LKDestinationAirportID").val(),
            //"PersonsAboard": $("#PersonsAboard").val(),
            //"PersonsSeriouslyInjured": $("#PersonsSeriouslyInjured").val(),
            //"PersonsKilled": $("#PersonsKilled").val(),
            //"PersonsWithMinorInjury": $("#PersonsWithMinorInjury").val(),
            "LKIncidentTypeID": $("#LKIncidentTypeID").val(),
            "PrevailingWeatherConditions": $("#PrevailingWeatherConditions").val(),
            "ExtendOfDamageToAircraft": $("#ExtendOfDamageToAircraft").val(),
            "DamagesToGroundObjects": $("#DamagesToGroundObjects").val(),
            "DescriptionOfExplosivesAndDangerousArticles": $("#DescriptionOfExplosivesAndDangerousArticles").val(),
            "LKOccurrenceInformerID": $("#LKOccurrenceInformerID").val(),
            "Notifier": $("#Notifier").val(),
            "PlaceofIncident": $("#PlaceofIncident").val(),
            "Notifier_Contact": $("#Notifier_Contact").val(),
            "DIsecurityNotify": $("#DIsecurityNotify:checked").val(),
            "DatetimeofCall": changeDateFormat($("#NotifierCallDate").val()),
            "DescriptionOfOccurrence": $("#DescriptionOfOccurrence").val(),
            "LKOccurrenceStatusTypeId": 1,
            "NotificationDescription": $("#NotificationDescription").val(),
            "NotificationId": $("#NotificationId").val()
        };
        console.log(person);

        $.ajax({

            url: "/Notifications/OccurenceNotify",
            type: "Post",
            data: JSON.stringify(person),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data > 0) {
                    window.location.href = '/home/Dashboard';
                }
                //else if (data === "Format") {
                //    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i> Invalid Notification File No Format </div>');
                //    $('.alert-danger').show();
                //    setTimeout(function () { $(".alert").hide(); }, 2000);
                //}
                //else if (data === "Exist") {
                //    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i> Notification File No Already Exist </div>');
                //    $('.alert-danger').show();
                //    setTimeout(function () { $(".alert").hide(); }, 2000);
                //}
            },
            error: function (msg) { alert(msg); }
        });
    });

    $("#BtnNotify").click(function () {
        data = validateNotification();
        if (data.code == true) {
            $(".btn-save-init,.btn-notify").hide();
            $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
            $('.alert-info').show();
            $("#new_occurrence").submit();
        } else {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
            $('.alert-danger').show();

            setTimeout(function () { $(".alert").hide(); }, 5000);
        }
    });

    $("#add_team_member").click(function (e) {
        $("#team_management").submit();
    });

    $("#radDISug").click(function (e) {       
        var checkedradio = $('[name="LKIncidentTypeID"]:radio:checked').val();          
        if (checkedradio !== "11") {
            $("#hideoccurclassification").hide(); 
            $("#OtherOccurClassification").val('');
        }
        else {
            $("#hideoccurclassification").show();
        }
    });

    $("#Change_IncidentType").click(function (e) {       
        var checkedradio = $('[name="LKIncidentTypeID"]:radio:checked').val();          
        if (checkedradio !== "11") {
            $("#hideoccurclassification").hide(); 
            $("#OtherOccurClassification").val('');
        }
        else {
            $("#hideoccurclassification").show();
        }
    });

    $("#Nameofforigninv").click(function (e) {
        var checkname = $('[name="InvestigationType"]:radio:checked').val();
        if (checkname === "21") {
            $("#hideInvestigatorName").show();          
        }
        else {
            $("#hideInvestigatorName").hide();
            $("#NameOfForeignInvestigator").val('');
        }
    });

    //$("#buttoneventsave").click(function () {

    //    alert("hi");
    //    $("#event3").submit();

    //});


    //$("#buttoneventsave").click(function (e) {
    //    alert("hi");
    //    event.preventDefault();
    //    var form = $('#event3')[0];

    //    var data = new FormData(form);
    //    alert(data);
    //    $.ajax({
    //        type: "POST",
    //        enctype: 'multipart/form-data',
    //        url: "/task/savefiles",
    //        data: data,
    //        processData: false,
    //        contentType: false,
    //        cache: false,
    //        timeout: 600000,
    //        success: function (data) {
    //            window.location.href = '/home/Dashboard'

    //        },
    //        error: function (msg) { alert(msg); }

    //    });
    //});

    //    $('#event3')
    //        .submit(function (e) {
    //            $.ajax( {
    //                url: "/task/savefiles",
    //                    type: 'POST',
    //                        data: new FormData(this),
    //                            processData: false,
    //                                contentType: false
    //            } );
    //    e.preventDefault();
    //});

    //$("#event3").submit(function (e) {
    //    e.preventDefault();
    //    var formData = new FormData(this);
    //    console.log(formData);
    //    $.ajax({
    //        url: "/task/savefiles",
    //        type: 'POST',
    //        data: formData,
    //        success: function (data) {
    //            alert(data)
    //        },
    //        cache: false,
    //        contentType: false,
    //        processData: false
    //    });
    //});


    $("#seachscope").click(function (e) {

        var status = $("#searches").val();
        $.ajax({
            url: '/task/searched',
            type: "Post",
            datatype: 'application/json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                status: status
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                window.location.href = '/home/Dashboard'

            },
            error: function (msg) { alert(msg); }
        });

    });

    $(".task_complete").click(function (e) {

        var id = $(this).attr('id').split('_');
        var investigationfileid = $("#InvFileID").val();
        if (id[1]) {
            var task_id = id[1];
            var event_id = id[2];
            var form_id = 'event' + event_id + '_form';
            //$('#event' + event_id + '_form .mandatory').each(
            //    function (index) {
            //        var input = $(this);
            //        if (input[0].tagName == 'TEXTAREA') {
            //            if (input.val() == '') {
            //                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Enter text.</div>');
            //                $('.alert-danger').show();
            //                setTimeout(function () { $(".alert").hide(); }, 3000);
            //                return false;

            //            }
            //            if (input[0].tagName == 'TEXTBOX') {
            //                if (input.val() == '') {
            //                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Enter text.</div>');
            //                    $('.alert-danger').show();
            //                    setTimeout(function () { $(".alert").hide(); }, 3000);
            //                    return false;

            //                }
            //            }
            //        }
            //    }
            //);


            return false;
            $.ajax({

                url: '/task/EventTaskComplete',
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                data: JSON.stringify({
                    investigationfileid: +investigationfileid,
                    task_id: +task_id
                }),
                success: function (result) {
                    if (result == 'success') {

                        $('.alert-success').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Task Completed Successfully</div>');
                        $('.alert-success').show();
                        setTimeout(function () { window.location.reload(); }, 2000);


                    }
                }

            });

        }
    });
    $(".view-tag-delete").click(function (e) {
        e.preventDefault();
        var attachid = $(this).data("attachid");
        var filename = $(this).data("filename");

        $("#deleted-" + filename).val(function (i, val) {
            return val + (!val ? '' : ', ') + attachid;
        });
        $("#filedatadiv-" + attachid).remove();


    });

});

function validateNotification() {
    if ($("#NotificationId").val() == '') {
        return { msg: "Enter Notification Number", code: false };
    }   
    else if ($("#PlaceofIncident").val() == '') {
        return { msg: "Enter Location of Occurrence", code: false };
    }   
    else if ($("#NotifierCallDate").val() == '') {
        return { msg: "Select Local Date and Time ", code: false };
    }
    else if ($("#LocalDateAndTimeOfOccurrence").val() == '') {
        return { msg: "Select Date And Time Of Occurrence", code: false };
    }
    else if ($("#OccurrenceDateTime").val() == '') {
        return { msg: "Select Occurrence Date and Time Local", code: false };
    }
    else if ($("#DescriptionOfOccurrence").val() == '') {
        return { msg: "Enter Occurrence Description", code: false };
    }
    else if ($("#Notifier").val() == '') {
        return { msg: "Enter Notified By ", code: false };
    }  
    else {
        return { code: true };
    }
}

function getallairportregistration(element) {
    $.ajax
        ({
            url: '/Notifications/getAllAirport',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKAircraftRegistrationID_" + element).append
                        ($('<option></option>').val(item.LKAircraftRegistrationID).html(item.LKAircraftRegistrationName));

                    $("#addAircraftRegistration_" + element).html('Add/Edit Airport');

                });
            },
            error: function () {
                alert("Something went wrong..");
            }
        });
}

function getAirportdetailsByairportId(e) {

    var ctrlId = e.id.split('_')[1];

    RegId = e.value;

    if (RegId === "") { RegId = parseInt(0); }

    $.ajax
        ({
            url: '/Notifications/getAirportDetailsByAirportId',
            data: { RegId: parseInt(RegId) },
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {

                $("#LKAirlineOperatorID_" + ctrlId).html('');
                $("#LKAircraftModelID_" + ctrlId).html('');
                $("#AircraftCategoryId_" + ctrlId).html('');

                if (RegId === 0) {

                    $("#addAircraftRegistration_" + ctrlId).html('Add Registration');

                    $("#LKAirlineOperatorID_" + ctrlId).append
                        ($('<option></option>').val(0).html(''));               
                }
                else {
                    $.each(JSON.parse(result), function (key, item) {

                        if ($("#LKAircraftRegistrationID_" + ctrlId).val() > 0) {

                            $("#addAircraftRegistration_" + ctrlId).html('Edit Registration');
                        }
                        else {
                            $("#addAircraftRegistration_" + ctrlId).html('Add Registration');
                        }

                        $("#LKAirlineOperatorID_" + ctrlId).append
                            ($('<option></option>').val(item.LKAirlineOperatorID).html(item.LKAirlineOperatorName));

                        $("#LKAircraftModelID_" + ctrlId).append
                            ($('<option></option>').val(item.LKAircraftModelID).html(item.LKAircraftModelName));                       
                    });
                }
            },
            error: function () {
            }
        });
}

function SaveAirportName() {
    var AirportID = $("#LKAirportDetailID").val();
    var Airport_Name = $("#AirportName").val();

    var ICAO_code = $("#ICAO").val();
    if (ICAO_code === '') {
        $('.popup_danger').html('Please Enter ICAO Code');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }   
    if (Airport_Name === '') {
        $('.popup_danger').html('Please Enter Airport Name');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }   
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Notifications/saveAirportDetails',
            dataType: 'json',
            data: {
                  LKAirportDetailID: parseInt(AirportID)               
                , ICAO: ICAO_code
                , AirportName : Airport_Name
            },
            error: function (err) {
                $("#Airport_Registration").modal('hide');
            },
            success: function (result) {
                if (result !== 0) {                    
                    $("#Airport_Registration").modal('hide');
                }
                else {
                    $('.popup_danger').html('Airport Name Already Exists');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                    return false;
                }
            }
        });
    }
}

function SaveOccurrenceCategory() {
    var CategorizationId = $("#LKOccurrenceCategorizationId").val();
    var Acronym = $("#OccurrenceCategorizationAcronym").val();
    var Categorization = $("#OccurrenceCategorization").val();
      
    if (Acronym === '') {
        $('.popup_danger').html('Please Enter Occurrence Category Acronym ');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    if (Categorization === '') {
        $('.popup_danger').html('Please Enter Occurrence Category ');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Notifications/AddOccurCategory',
            dataType: 'json',
            data: {
                LKOccurrenceCategorizationId: parseInt(CategorizationId)
                , OccurrenceCategorization: Categorization
                , OccurrenceCategorizationAcronym: Acronym
            },
            error: function (err) {
                $("#OccurCateg_Registration").modal('hide');
                $("#OccurrenceCategorizationId").append($('<option></option>').val(CategorizationId).html(Categorization));               
            },
            success: function (result) {
                if (result !== 0) {
                   // getAllOccurrenceCategory();
                    $("#OccurCateg_Registration").modal('hide');
                }
                else {
                    $('.popup_danger').html('Category Name Already Exists');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                    return false;
                }
            }
        });
    }
}