﻿$(document.body).on('change', '.Rptupload', function () {
    var img = $(this).val();
    var fileType = img.substring((img.lastIndexOf('.') + 1), img.length);
    if (fileType == 'doc' || fileType == 'docx' || fileType == 'pdf') {
        var num = $(this).attr('id').split('_');
        var file_number = num[1];
        $("#RptuploadFile_" + file_number).val(img);
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Only Word and PDF files are allowed.Please choose a different file.</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
        $("#RptuploadFile_0").val('');
        $('#RptuploadFile_0').focus();
    }
});
function saveReports() {
    var regep = /^\d+\.\d{1,2}$/;
    var ok = regep.test($("#FileVersion").val());
    if ($("#RptuploadFile_0").val() == "" || $("#RptuploadFile_0").val() == null) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file to upload</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
        $('#RptuploadFile_0').focus();
    }
    else if ($("#FileVersion").val() == "" || $("#FileVersion").val() == null) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter file version</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
        $('#FileVersion').focus();
    }
    else if (!ok) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter a valid file version</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
        $('#FileVersion').focus();

    }
    else if (ok && $("#FileVersion").val().length > 3) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter a valid file version</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
        $('#FileVersion').focus();
    }
    else {
        $.ajax({
            url: "/Forms/checkVersionExist",
            type: "POST",
            async: false,
            data: { id: $("#InvestigationFileID").val(), version: $("#FileVersion").val() },
            success: function (data) {
                if (!data) {
                    var count = $('.Rptupload').length;
                    var file_Info = new Array(count);
                    if (count > 0) {
                        //var formdata = new FormData();
                        //if ($('.Rptupload')[0].files.length > 0) {
                        //    for (var i = 0; i < count; ++i) {
                        //        file_Info[i] = new Array(50);
                        //        var inp = $('.Rptupload')[i];
                        //        //var hdn = $('.' + hdnVal)[i];
                        //        for (var j = 0; j < inp.files.length; j++) {
                        //            file_Info[i][0] = inp.files[j].name;
                        //            var f = inp.files[j];
                        //            formdata.append(inp.files[j].name, inp.files[j]);
                        //        }
                        //    }
                        //    var xhr1 = new XMLHttpRequest();
                        //    xhr1.open('POST', '/Forms/SaveFiles?InvId=' + $("#InvestigationFileNumber").val());
                        //    xhr1.send(formdata);
                        //}
                    }
                    $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
                    $('.alert-info').show();
                    $('#btnUploadReports').hide();
                    $("#Rpt_add_attachment").hide();
                    $("#UploadReports").submit();
                    setTimeout(function () { $(".alert").hide(); }, 8000);
                }
                else {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>This file version already exists.Please give an updated version.</div>');
                    $('.alert-danger').show();
                }
            }
        });
    }
}

//$('#FileVersion').keypress(function (event) {
//    //var ok = /^\d*\.?\d{0,2}$/.test(event.target.value);//allows multiple dots
//    //var ok = /^\d+( \.?\d{1,2})?$/.test(event.target.value);//doesnot work on first condition val=''

//    //var ok = /^\d*(\.\d{0, 2})?$/.test(event.target.value);//doesnot allow digits after dot
//    var value = event.key;
//    var testreg = /^\d*\.?\d{0,2}$/;
//    if (!testreg.test(value)) {
//        event.preventDefault();       
//    }    
//});

function checkDecimal(th, event) {

    var val = th.value;
    var testreg = /^\d*\.?\d{0,2}$/;
    var evt = (event) ? event : window.event;
    var preskey = event.key;
    var charCode = (evt.keyCode) ? evt.keyCode : evt.which;
    if (preskey == '.') {
        if (val.indexOf('.') < 0)
            return true;
        else
            return false;
    }
    else if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)) {
        return false;
    } else if (charCode == 101) {
        return false;

    }

    return true;
}