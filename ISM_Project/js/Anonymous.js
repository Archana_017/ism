﻿var i = 1;
var fileupload = true;
$("#corres_add_attachment").click(function (e) {
    var controlcount = $('.corresupload').length;
    for (var ii = 0; ii < controlcount; ii++) {
        var inp = $('.corresupload')[ii];
        if (inp.files.length == 0) {
            fileupload = false;
            break;
        }
        else
            fileupload = true;
    }
    if (fileupload == false) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
    }
    else {
        var html = '<div class="col-sm-12 col-form-box no-padding" style="margin-bottom: 15px;"><div class="input-group" ><input id="corresuploadFile_' + i + '" class="form-control" placeholder= "Choose File" disabled= "disabled" > <div class="input-group-btn"><div class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="corresuploadBtn_' + i + '" type="file" class="upload corresupload" name="CorresFiles[]" required="required"><input type="hidden" id="hdnVal_' + i + '" class="hdnfile"/></div></div ></div></div > ';
        $(".attachments-tag").append(html);
        i++;
    }
});

$(document.body).on('change', '.corresupload', function () {
    var img = $(this).val();
    var num = $(this).attr('id').split('_');
    var file_number = num[1];
    $("#corresuploadFile_" + file_number).val(img);
});

function saveCorrespondence(fileElementId) {
    data = validateForm();
    if (data.code == true) {
        var count = $('.' + fileElementId).length;
        var file_Info = new Array(count);
        var formReceipt = new FormData();
        $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
        $('.alert-info').show();
        $("#btnSaveCorres").hide();
        $("#new_corrspondence").submit();
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
    }
}
function validateForm() {
    if ($("#HdnMode").val() == "response") {
        if ($("#CorrespondenceContent").val() == '') {
            $("#CorrespondenceContent").focus();
            return { msg: "Please enter Comments", code: false };
        }
        if (!validateEmail($("#CorrespondenceMessageResponseFrom").val())) {
            return { msg: "Please enter a valid email address", code: false };
        }
        else {
            return { msg: "", code: true };
        }
    }
}
function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true);
    }
    return (false)
}
function updateSRHistory() {
    if ($("#SRCommitteeComments").val() == '') {
        $("#SRCommitteeComments").focus();
        //return { msg: "Please enter Comments", code: false };
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter Comments</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
    }
    else if ($("#SRCommitteeClosureStatus").val() == 'N/A'){
        $("#SRCommitteeClosureStatus").focus();
        //return { msg: "Please enter Comments", code: false };
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please select closure approval status</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
    }
    else {
        $('#SR_History').submit();
    }
}