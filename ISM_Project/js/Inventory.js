﻿function saveInventory() {
    var isFormValid = validation();
    if (isFormValid.code == true) {
        var Expirydate = changeDateOnlyFormat($("#ExpiryDate").val());
        $("#ExpiryDate").val(Expirydate);
        $('#new_inventory').submit();
        $('#btnSaveInventory').hide();
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + isFormValid.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
}
function validation() {
    if ($('#CategoryName').val() == '') {
        $("#CategoryName").focus();
        return { msg: "Please enter catergory name", code: false };
    }
    if ($('#Item').val() == '') {
        $("#Item").focus();
        return { msg: "Please enter item", code: false };
    }
    if ($('#ItemNo').val() == '') {
        $("#ItemNo").focus();
        return { msg: "Please enter item no", code: false };
    }
    if ($('#Description').val() == '') {
        $("#Description").focus();
        return { msg: "Please enter description", code: false };
    }
    if ($('#PartNo').val() == '') {
        $("#PartNo").focus();
        return { msg: "Please enter part no", code: false };
    }
    if ($('#SNo').val() == '') {
        $("#SNo").focus();
        return { msg: "Please enter serial no", code: false };
    }
    if ($('#Quantity').val() == '') {
        $("#Quantity").focus();
        return { msg: "Please enter quantity", code: false };
    }
    if ($('#QuantityType').val() == '') {
        $("#QuantityType").focus();
        return { msg: "Please enter quantity type", code: false };
    }
    if ($('#Availability').val() == '') {
        $("#Availability").focus();
        return { msg: "Please enter availability", code: false };
    }
    if ($('#ExpiryDate').val() == '') {
        $("#ExpiryDate").focus();
        return { msg: "Please select expiry date", code: false };
    }
    if ($('#ExpiredItems').val() == '') {
        $("#ExpiredItems").focus();
        return { msg: "Please enter expired items", code: false };
    }
    if ($('#RequiredItems').val() == '') {
        $("#RequiredItems").focus();
        return { msg: "Please enter required no of items", code: false };
    }
    else {
        return { msg: "", code: true };
    }
}
$(document.body).on('change', '.invupload', function (e) {
    var img = $(this).val();
    var num = $(this).attr('id').split('_');
    var file_number = num[1];
    var fileType = img.substring((img.lastIndexOf('.') + 1), img.length);
    if (fileType == 'jpeg' || fileType == 'jpg' || fileType == 'png' || fileType == 'bmp') {
        $("#invnuploadFile_" + file_number).val(img);
        var files = e.target.files;
        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }
                $.ajax({
                    type: "POST",
                    url: '/Admin/UploadInvnFile?invID=0',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        console.log(result);
                    },
                    error: function (xhr, status, p3, p4) {
                        var err = "Error " + " " + status + " " + p3 + " " + p4;
                        if (xhr.responseText && xhr.responseText[0] == "{")
                            err = JSON.parse(xhr.responseText).Message;
                        console.log(err);
                    }
                });
            } else {
                alert("This browser doesn't support HTML5 file uploads!");
            }
        }
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Only images are allowed.Please choose a different file.</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 3000);
        $(this).val('');
        $(this).focus();
    }
});
var i = 1;
var fileupload = true;
$("#invn_add_attachment").click(function (e) {
    var controlcount = $('.invupload').length;
    for (var ii = 0; ii < controlcount; ii++) {
        var inp = $('.invupload')[ii];
        if (inp.files.length == 0) {
            fileupload = false;
            break;
        }
        else
            fileupload = true;
    }
    if (fileupload == false) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
    else {
        var html = '<div class="col-sm-12 col-form-box no-padding" style="margin-bottom: 15px;" id="div_fileupload_' + i + '"><div class="input-group" ><input id="invnuploadFile_' + i + '" class="form-control" placeholder= "Choose File" disabled= "disabled" > <div class="input-group-btn"><div id="FileUpload_' + i + '" class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="invnuploadBtn_' + i + '" type="file" class="upload invupload" name="InvenFiles[]" required="required"><input type="hidden" id="hdnVal_' + i + '" class="hdnfile"/></div><a href="javascript:void(0);" class="remove_Attached_file" onclick=removeInvnFiles(this) id="RemoveAttach_' + i + '"><i class="fa fa-times" aria-hidden="true"></i></a></div></div></div >';
        //<div><a id="RemoveAttach_' + i + '" class="remove_CorresFileattachment" onclick=removeCorresFiles(this)>Remove</a></div>
        $(".attachments-tag").append(html);
        i++;
    }
});
function removeInvnFiles(e) {
    var attach_ids = $(e).attr('id').split('_');
    var attach_name = $('#invnuploadFile_' + parseInt(attach_ids[1])).val();
    //var attachurl = $(this).prev().attr('href');
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/Admin/RemoveInventoryImages',
        dataType: 'json',
        data: { filename: (attach_name), Invn_ID: 0 },
        error: function (err) {

        },
        success: function (result) {
            $('#invnuploadFile_' + parseInt(attach_ids[1])).remove();
            $('#invnuploadBtn_' + parseInt(attach_ids[1])).remove();
            $('#RemoveAttach_' + parseInt(attach_ids[1])).remove();
            $('#FileUpload_' + parseInt(attach_ids[1])).remove();
            $('#div_fileupload_' + parseInt(attach_ids[1])).remove();
        }
    });
}
$(".remove_Inv_attachment").click(function (e) {
    e.preventDefault();
    var attach_ids = $(this).attr('id').split('_');
    var attach_id = attach_ids[1];
    var attachurl = $(this).prev().attr('src');
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/Admin/DeleteImages',
        dataType: 'json',
        data: { id: parseInt(attach_id), path: attachurl },
        error: function (err) {

        },
        success: function (result) {
            $('#attach_' + attach_id).remove();
            $('#img_' + attach_id).remove();
        }
    });
});
//to update the quantity for maintaining the log
$(document.body).on('click', '.ChangeQuantity', function () {
    $("#Change_Quantity").modal('show');
});

$(document.body).on('click', '.UpdateInventoryUse', function () {
    $("#Update_InventoryUSe").modal('show');
});

function UpdateQuantity() {
    var REFILLED = $("#REFILLED").val();
    var AVAILABLE = $("#AVAILABLE").val();
    var InventoryID = $("#InventoryID").val();
    var QuantityType = $('#QuantityType').val();
    if (REFILLED == '') {
        $('.popup_danger').html('Please enter the refilled quantity');
        $('.popup_danger').css({ display: "block" });
        $("#REFILLED").focus();
        setTimeout(function () { $(".popup_danger").hide(); }, 6000);
    }
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Admin/UpdateQuantityHistory',
            dataType: 'json',
            data: { Available: AVAILABLE, Refilled: REFILLED, inventoryID: InventoryID, InvQuanType: QuantityType },
            error: function (err) {
                $("#Change_Quantity").modal('hide');
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in updating the quantity.</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 2000);
            },
            success: function (result) {
                if (result == "success") {
                    $('#btnSaveInventory').hide();
                    $("#Change_Quantity").modal('hide');
                    $('.alert-success').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Quantity updated successfully.</div>');
                    $('.alert-success').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000)
                }
                else {
                    $("#Change_Quantity").modal('hide');
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in updating the quantity.</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
            }
        });
    }
}
function UpdateInvnUse() {
    var Used = $("#QunatityUsed").val();
    var Totalquantity = parseInt($("#AVAILABLE").val());
    var InventoryID = parseInt($("#InventoryID").val());
    var QuantityType = $('#QuantityType').val();
    var InvestigationID = $('#InvestigationID').val();
    var NotificationID = $('#NotificationID').val();
    var UsageDat = $('#UsageDate').val();
    var Remarks = $('#Remarks').val();
    var AvailableQuan = $('#AVAILABLE').val();
    if (Used == '') {
        $('.popup_danger').html('Please enter the quantity');
        $('.popup_danger').css({ display: "block" });
        $("#QunatityUsed").focus();
        setTimeout(function () { $(".popup_danger").hide(); }, 2000);
    }
    else if ((parseInt(Used)) > parseInt(AvailableQuan)) {
        $('.popup_danger').html('Available is ' + parseInt(AvailableQuan) + ' ' + $('#QuantityType').val()+'. Please update the used quantity accordingly.');
        $('.popup_danger').css({ display: "block" });
        $("#QunatityUsed").focus();
        setTimeout(function () { $(".popup_danger").hide(); }, 3000);
    }
    //else if (InvestigationID == '' && NotificationID == '') {
    //    $('.popup_danger').html('Please enter the Investigation Number');
    //    $('.popup_danger').show();
    //    $("#InvestigationNumber").focus();
    //    setTimeout(function () { $(".popup_danger").hide(); }, 2000);
    //}
    else {
        if (InvestigationID == '')
            InvestigationID = '0';
        if (UsageDat == null || UsageDat == '')
            UsageDat = null;
        else
            UsageDat = changeDateOnlyFormat(UsageDat)
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Admin/UpdateInventoryUsage',
            dataType: 'json',
            data: { Used: parseInt(Used), quantity: Totalquantity, inventoryID: InventoryID, InvQuanType: QuantityType, InvestigationID: parseInt(InvestigationID), UsedDate: UsageDat, Remarks: Remarks },
            error: function (err) {
                $("#Update_InventoryUSe").modal('hide');
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in updating the quantity.</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 2000);
            },
            success: function (result) {
                if (result == "success") {
                    $('#btnSaveInventory').hide();
                    $("#Update_InventoryUSe").modal('hide');
                    $('.alert-success').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Inventory Usage updated successfully.</div>');
                    $('.alert-success').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000)
                }
                else {
                    $("#Update_InventoryUSe").modal('hide');
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in updating the quantity.</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
            }
        });
    }
}
