﻿
$(document).ready(function () {

    if ($("#InvTabIndex").length > 0) {
        var selected_tab = $("#InvTabIndex").val();
        $("a[href='#" + selected_tab + "']").tab("show");
    }

    var i = 1;

    $(document.body).on('click', '.remove_form35aircraft', function () {
        var aircraft_id = $(this).attr('id').split('_');
        if (aircraft_id[2]) {
            var acid = parseInt(aircraft_id[2]);
            $.ajax({
                type: 'POST',
                cache: false,
                url: '/DI/RemoveAircraftDetails',
                datatype: 'json',
                data: { air_id: parseInt(acid) },
                success: function (data) {
                    if (data == 'success') {
                        $("#acraft_details_id_" + aircraft_id[1]).remove();
                        var old_count = $("#aircrafts_inv_no").html();
                        var new_count = parseInt(old_count) - parseInt(1);
                        var ncount = parseInt(new_count);
                        $("#aircrafts_inv_no").html(ncount);
                        $("#NoOfAircraftInvolved").val(ncount);
                        $('.alert-success').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Aircraf Details Removed Successfully</div>');
                        $('.alert-success').show();
                        $('.alert-success').hide();
                        //setTimeout(function () { window.location.reload(); }, 5000);                        
                    }
                },
                error: function (msg) { alert(msg); }
            });
        }
    });


    //$("#occ_add_attachment").click(function (e) {

    //    var html = '<div class="col-sm-12 col-form-box no-padding" style="margin-bottom: 15px;"><div class="input-group" ><input id="uploadFile_' + i + '" class="form-control" placeholder= "Maximum file size limited to 20MB" disabled= "disabled" > <div class="input-group-btn"><div class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="uploadBtn_' + i + '" type="file" class="upload" required="required"><input type="hidden" id="hdnVal_' + i + '" class="hdnfile"/></div></div ></div></div> ';
    //    $(".attachments-tag").append(html);
    //    i++;
    //});

    $(document.body).on('change', '.upload', function (e) {
        var img = $(this).val();
        var num = $(this).attr('id').split('_');
        var file_number = num[1];
        $("#uploadFile_" + file_number).val(img);

        // Changes- start on 16 aug 2018 
        //$("#general_info").submit();
        var files = e.target.files;
        //var myID = 3; //uncomment this to make sure the ajax URL works
        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }

                $.ajax({
                    type: "POST",
                    url: '/DI/UploadFile',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        console.log(result);
                    },
                    error: function (xhr, status, p3, p4) {
                        var err = "Error " + " " + status + " " + p3 + " " + p4;
                        if (xhr.responseText && xhr.responseText[0] == "{")
                            err = JSON.parse(xhr.responseText).Message;
                        console.log(err);
                    }
                });
            } else {
                alert("This browser doesn't support HTML5 file uploads!");
            }
        }

        // Changes - End on 16 aug 2018 

    });

    $(document.body).on('change', '.EveFileupload', function (e) {
        var img = $(this).val();
        var num = $(this).attr('id').split('_');
        var file_number = num[1];
        $("#uploadFile_" + file_number).val(img);

        var files = e.target.files;

        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }

                $.ajax({
                    type: "POST",
                    url: '/Task/EvidenceFileupload',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        console.log(result);
                    },
                    error: function (xhr, status, p3, p4) {
                        var err = "Error " + " " + status + " " + p3 + " " + p4;
                        if (xhr.responseText && xhr.responseText[0] == "{")
                            err = JSON.parse(xhr.responseText).Message;
                        console.log(err);
                    }
                });
            } else {
                alert("This browser doesn't support HTML5 file uploads!");
            }
        }

    });

    $(document.body).on('change', '#ClassName', function () {
        var theme_id = $("#ClassName :selected").val();
        var new_theme = 'fixed-nav sticky-footer bg-dark ' + theme_id;
        $('body').removeClass();
        $('body').addClass(new_theme);
    });

    $("#Btnform35submit").click(function () {
        data = validateForm35();
        if (data.code == true) {
            var iv_id = $("#InvestigationFileID").val();
            if (iv_id == 0) {
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please save data before submitting Form 035.</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 2000);
                return false;
            } else {
                $(".btn-SaveGn,#Btnform35submit").hide();
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '/DI/ValidateForm35',
                    dataType: 'json',
                    data: { invId: parseInt(iv_id) },
                    error: function (err) {

                    },
                    success: function (result) {
                        if (result == 1) {
                            $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
                            $('.alert-info').show();
                            setTimeout(function () { $(".alert-info").hide(); }, 1000);
                            setTimeout(function () { $("#form35submit").submit(); }, 1000);
                            //$('.alert-success').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Form 035 submitted successfully.</div>');
                            //$('.alert-success').show();
                            //setTimeout(function () { $(".alert-success").hide(); }, 3000);
                        } else {
                            $(".btn-SaveGn,#Btnform35submit").show();
                            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please complete all mandatory fields and Save before submitting Form 035.</div>');
                            $('.alert-danger').show();
                            setTimeout(function () { $(".alert").hide(); }, 2000);
                        }
                    }
                });
            }
        }
        else {

            $("a[href='#" + TabIndex + "']").tab("show");

            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
            $('.alert-danger').show();

            setTimeout(function () { $(".alert").hide(); }, 2000);

            $("#InvTabIndex").val(TabIndex);
        }
    });

    $(".remove_attachment").click(function (e) {
        e.preventDefault();
        var attach_ids = $(this).attr('id').split('_');
        var attach_id = attach_ids[1];
        var attachurl = $(this).prev().attr('href');
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/DI/DeleteAttachment',
            dataType: 'json',
            data: { id: parseInt(attach_id), path: attachurl },
            error: function (err) {

            },
            success: function (result) {
                $('#attach_' + attach_id).remove();
                $('#file_' + attach_id).remove();
            }
        });
    });

    $('.remove_Fileattachment').click(function () {

        var attach_ids = $(this).attr('id').split('_');
        var attach_id = attach_ids[1];
        var attachurl = $(this).prev().attr('href');
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/DI/RemoveAttachedFile',
            dataType: 'json',
            data: { id: parseInt(attach_id) },
            error: function (err) {

            },
            success: function (result) {
                $('#uploadFile_' + attach_id).remove();
                $('#uploadBtn_' + attach_id).remove();
                $('#RemoveAttach_' + attach_id).remove();
            }
        });
    });

    $(".remove_Evidenceattachment").click(function (e) {
        e.preventDefault();
        var attach_ids = $(this).attr('id').split('_');
        var attach_id = attach_ids[1];
        var attachurl = $(this).prev().attr('href');
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Task/DeleteEvidenceAttachment',
            dataType: 'json',
            data: { id: parseInt(attach_id), path: attachurl },
            error: function (err) {

            },
            success: function (result) {

                $('#attach_' + attach_id).remove();
                $('#file_' + attach_id).remove();
            }
        });
    });

    $("#BtnInvestigationScope").click(function (e) {
        e.preventDefault();
        $("#BtnInvestigationScope").hide();
        var sList = [];
        $('input[type=checkbox]').each(function () {
            var id_part = $(this).attr('id').split('_');
            var sThisVal = (this.checked ? "1" : "2");
            $("#invScopeStatus_" + id_part[1]).val(sThisVal);
            //$(this).prop('checked', true);
            //sList.push(sThisVal);
        });
        $("#formDefn_Inv_ScopeSubmit").submit();
    });

    $("#BtnMultipleAssignTeammember").click(function (e) {

        e.preventDefault();

        var tm_id = $('[name="TeamMemberId[]"]:checked').val();

        var exp_date = $("#DateTimeofCompletion").val();

        if (exp_date == '') {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Expected Date Of Completion.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
            return false;
        }
        else if (tm_id == null) {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Team Member.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
            return false;
        }
        else {
            $(this).hide();
            //$('[name="TeamMemberId[]"]:checked').val();
            $("#DateTimeofCompletion").val(changeDateOnlyFormat(exp_date));
            $("#formAssigning_team_members").submit();
        }
    });

    $("#btnChangeDate").click(function (e) {

        e.preventDefault();

        var exp_date = $("#DateTimeofCompletion").val();

        if (exp_date == '') {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Expected Date Of Completion.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
            return false;
        }
        else {
            $(this).hide();
            //$('[name="TeamMemberId[]"]:checked').val();
            $("#DateTimeofCompletion").val(changeDateOnlyFormat(exp_date));
            $("#formChange_CompletionDate").submit();
        }
    });

    $("#BtnAssignTeammember").click(function (e) {

        e.preventDefault();

        var tm_id = $('[name="TeamMemberId"]:checked').val();

        var exp_date = $("#DateTimeofCompletion").val();

        if (exp_date == '') {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Expected Date Of Completion.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
            return false;
        }
        else if (tm_id == null) {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Team Member.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
            return false;
        }
        else {
            $('[name="TeamMemberId"]:checked').val();
            $("#DateTimeofCompletion").val(changeDateOnlyFormat(exp_date));
            $("#formAssigning_team_members").submit();
        }
    });

    $("#BtnReAssignTeammember").click(function (e) {

        e.preventDefault();
        var tm_id = $('[name="multipleUnassignedTeam_mbr_id[]"]:checked').val();
        var tm_assigned = $('[name="multipleAssignedTeam_mbr_id[]"]:checked').val();
        var rows = $('#currentmembers tr').length - 1;// newly added line for jira ticket multiple team member removal starts here
        var checkboxtickcount = $('input[name="multipleAssignedTeam_mbr_id[]"]:checked').length;

        var assigned_tickcount = $('input[name="multipleUnassignedTeam_mbr_id[]"]:checked').length;

        var already_assigned = $('input[name="multipleAssignedTeam_mbr_id[]"]').length;

        if (assigned_tickcount == 0 && (checkboxtickcount >= already_assigned)) {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please assign a new member to proceed.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);

            return false; /* newly added code ends here*/
        }

        var Rem_Reason = $("#ReasonForTeammemberRemoval").val();
        if (tm_assigned != null) {
            if (Rem_Reason == '') {
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter reason for removal.</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 2000);
                return false;
            }
        }
        //else if (tm_id == null) {
        //    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Team Memebr.</div>');
        //    $('.alert-danger').show();
        //    setTimeout(function () { $(".alert").hide(); }, 3000);
        //    return false;
        //}
        //else {
        //  $('[name="TeamMemberId"]:checked').val();

        $("#formReAssigning_team_members").submit();
        //}

    });

    $("#BtnRemoveEvent").click(function (e) {

        e.preventDefault();

        var tgid = $("#InvestigationTaskGroupEventID").val();

        var Inv_Id = $("#InvestigationId").val();

        var Ctrl_frm = $("#ControlFrom").val();

        $(this).hide();

        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Task/RemoveTaskSubEvents',
            dataType: 'json',
            data: { InvId: parseInt(Inv_Id), TgEvtId: parseInt(tgid) },
            error: function (err) {
            },
            success: function (result) {
                if (result > 0) {

                    if (Ctrl_frm == 'IF') {
                        window.location.href = '/Task/InvestigationFlow/' + parseInt(result) + '?invid=' + parseInt(Inv_Id);
                    }
                    else {
                        window.location.href = '/Task/taskgroup/' + parseInt(result) + '?invid=' + parseInt(Inv_Id);
                    }
                } else {
                    return false;
                }
            }
        });
    });

    $(".tm_assign").click(function (e) {
        e.preventDefault();
        var tgid = $(this).attr('id');

        var Inv_Id = $("#InvFileId").val();

        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Task/ValidateTaskGroupEvent',
            dataType: 'json',
            data: { InvId: parseInt(Inv_Id), TgEvtId: parseInt(tgid) },
            error: function (err) {
            },
            success: function (result) {
                if (result == 1) {
                    window.location.href = '/Task/Assigning_team_members/' + parseInt(tgid) + '?invid=' + parseInt(Inv_Id) + '&CtrlFrom=TG';
                } else {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Assign previous event first.</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 3000);
                    return false;
                }
            }
        });
    });

    $(".if_tm_assign").click(function (e) {
        e.preventDefault();
        var tgid = $(this).attr('id');
        var Inv_Id = $("#InvFileId").val();
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Task/ValidateTaskGroupEvent',
            dataType: 'json',
            data: { InvId: parseInt(Inv_Id), TgEvtId: parseInt(tgid) },
            error: function (err) {

            },
            success: function (result) {
                if (result == 1) {
                    window.location.href = '/Task/Assigning_team_members/' + parseInt(tgid) + '?invid=' + parseInt(Inv_Id) + '&CtrlFrom=IF';
                } else {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Assign previous event first.</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                    return false;
                }
            }
        });

    });

    //$(document).on('change', '.ChartYear', function (e) {
    //    alert('hi');
    //});

    $('body').on('dp.change', ".chartdate", function (e) {
        e.stopPropagation();
        displayChart();
    });


    $("#CrewsWithFatalInjuryInfo,#CrewsWithSeriousInjuryInfo, #CrewsWithMinorInjuryInfo,#CrewsKilledInfo, " +

        "#CabincrewWithFatalInjuryInfo, #CabincrewWithSeriousInjuryInfo,#CabincrewWithMinorInjuryInfo,#CabincrewsWithotherInjuryInfo, " +

        "#OthercrewWithFatalInjuryInfo,#OthercrewWithSeriousInjuryInfo,#OthercrewWithMinorInjuryInfo,#OthercrewsWithotherInjuryInfo," +

        "#PassengersWithFatalInjuryInfo,#PassengersWithSeriousInjuryInfo,#PassengersWithMinorInjuryInfo,#PassengersKilledInfo," +

        "#OthersWithFatalInjuryInfo,#OthersWithSeriousInjuryInfo,#OthersWithMinorInjuryInfo,#OthersKilledInfo").change(function (e) {

            var iff = $("#CrewsWithFatalInjuryInfo").val();
            var iss = $("#CrewsWithSeriousInjuryInfo").val();
            var imm = $("#CrewsWithMinorInjuryInfo").val();
            var inn = $("#CrewsKilledInfo").val();
            var result = parseInt(iff) + parseInt(iss) + parseInt(imm) + parseInt(inn);
            $("#CrewsTotalInjury").val(result);

            var cff = $("#CabincrewWithFatalInjuryInfo").val();
            var css = $("#CabincrewWithSeriousInjuryInfo").val();
            var cmm = $("#CabincrewWithMinorInjuryInfo").val();
            var cnn = $("#CabincrewsWithotherInjuryInfo").val();
            var crew_result = parseInt(cff) + parseInt(css) + parseInt(cmm) + parseInt(cnn);
            $("#CabincrewsWithTotalInjuryInfo").val(crew_result);

            var off = $("#OthercrewWithFatalInjuryInfo").val();
            var oss = $("#OthercrewWithSeriousInjuryInfo").val();
            var omm = $("#OthercrewWithMinorInjuryInfo").val();
            var onn = $("#OthercrewsWithotherInjuryInfo").val();
            var othercrew_result = parseInt(off) + parseInt(oss) + parseInt(omm) + parseInt(onn);
            $("#OthercrewsWithTotalInjuryInfo").val(othercrew_result);

            var pff = $("#PassengersWithFatalInjuryInfo").val();
            var pss = $("#PassengersWithSeriousInjuryInfo").val();
            var pmm = $("#PassengersWithMinorInjuryInfo").val();
            var pnn = $("#PassengersKilledInfo").val();
            var passenger_result = parseInt(pff) + parseInt(pss) + parseInt(pmm) + parseInt(pnn);

            $("#PassengersTotalInjury").val(passenger_result);

            var otff = $("#OthersWithFatalInjuryInfo").val();
            var otss = $("#OthersWithSeriousInjuryInfo").val();
            var otmm = $("#OthersWithMinorInjuryInfo").val();
            var otnn = $("#OthersKilledInfo").val();
            var other_result = parseInt(otff) + parseInt(otss) + parseInt(otmm) + parseInt(otnn);
            $("#OthersTotalInjury").val(other_result);

            var totalonboard = parseInt(result) + parseInt(crew_result) + parseInt(othercrew_result) + parseInt(passenger_result);

            $("#TotalOnBoardInfo").val(totalonboard);

        });


    $('#SR_Date').on("blur", function (e) {

        var RegId = $("#TypeofsafetyRecommendation").val();

        if (RegId === "") { RegId = parseInt(0); }

        var sr_year = $("#SR_Date").val();

        $.ajax
            ({
                url: '/SR/getSRNumber',
                data: { id: parseInt($("#InvestigationFileID").val()), srtype: parseInt(RegId), sryear: parseInt(sr_year) },
                type: 'GET',
                datatype: 'application/json',
                contentType: 'application/json',
                success: function (result) {
                    $("#SafetyRecommendationNumber").val(result);
                    $("#SafetyRecommendationNumber").html(result);
                    $("#SafetyRecommendation_Number").val(result);
                    $("#SafetyRecommendation_Number").html(result);
                },
                error: function () {

                }
            });
    });

    $('#LKOccurrenceStatusTypeId').on("change", function () {
        $("#DIForm").submit();
    });

    $('#Occurrenceyear').on("blur", function (e) {
        $("#DIForm,#DAAIForm").submit();
    });

    $('#InvLKOccurrenceStatusTypeId').on("change", function () {
        $("#IICForm").submit();
    });
    $('#InvInvestigationId').on("change", function () {
        $("#IICForm").submit();
    });

    $('#LKOccurrenceStatusTypeId').on("change", function () {
        $("#DAAIForm").submit();
    });

    $('#InvOccurrenceyear').on("blur", function (e) {
        $("#IICForm,#DAAIForm").submit();
    });

    $('#TaskInvOccurrenceyear').on("blur", function (e) {
        $("#IICForm,#DAAIForm").submit();
    });

    $('#InvSearchtext').on("blur", function (e) {
        $("#IICForm,#DAAIForm").submit();
    });


    $('#InvLKOccurrenceStatusTypeId').on("change", function () {
        $("#DAAIForm").submit();
    });

    $('#EventConfirm-dialog').dialog({
        autoOpen: false, width: 400, resizable: false, modal: true,
        buttons: {
            "Yes": function (e) {                                                
                $(this).dialog("close");
                e.preventDefault();
                $('button').removeAttr("OnClick");
                return true;
            },
            "No": function () {
                $(this).dialog("close");
                return false;
            }
        }
    });
});

var TabIndex = 1;

function validateNumbersOnly(e) {
    var unicode = e.charCode ? e.charCode : e.keyCode;
    if ((unicode == 8) || (unicode == 9) || (unicode > 47 && unicode < 58)) {
        return true;
    }
    else {
        e.preventDefault();
        return false;
    }
}

function validateForm35() {
    if ($("#NotifierName").val() == '') {
        TabIndex = 1;
        return { msg: "Enter Notifier Name", code: false };
    }
    else
        if ($("#NotifierCallDate").val() == '') {
            TabIndex = 1;
            return { msg: "Enter Local Date and Time ", code: false };
        }
        else if ($("#UTCDateAndTimeOfOccurrence").val() == '') {
            return { msg: "Enter UTC Date And Time Of Occurrence", code: false };
        }
        else if ($("#LocalDateAndTimeOfOccurrence").val() == '') {
            return { msg: "Enter Local Date And Time Of Occurrence", code: false };
        }
        //else if ($("#LKCountryID").val() == '') {
        //    TabIndex = 1;
        //    return { msg: "Select Country Name", code: false };
        //}
        else if ($("#PlaceOfIncident").val() == '') {
            TabIndex = 1;
            return { msg: "Enter Location of Occurrence", code: false };
        }
        else if ($('input[name=LKIncidentTypeID]:checked').val() === "11" && $("#OtherOccurClassification").val() === '') {
            //if ($("#OtherOccurClassification").val() === '') {
            TabIndex = 2;
            this.focus();
            return { msg: "Enter Other Occurrence Classification Details", code: false };
            //}            
        }
        else if ($("#EventDescription").val() == '') {
            TabIndex = 2;
            return { msg: "Enter Occurrence Description", code: false };
        }
        else if ($('input[name=DutyInvestigatorSuggestions]:checked').length <= 0) {
            TabIndex = 6;
            return { msg: "Select Duty Investigator Suggestions", code: false };
        }
        else if ($('input[name=DutyInvestigatorFurtherSuggestion]:checked').length <= 0) {
            TabIndex = 6;
            return { msg: "Select Duty Investigator Further Suggestion", code: false };
        }
        else if ($('input[name=InvestigationType]:checked').val() === '21' && $("#NameOfForeignInvestigator").val() === '') {
            TabIndex = 6;
            return { msg: "Enter Name Of Foreign Investigator", code: false };
        }
        else {
            return { msg: "", code: true };
        }

    //if ($('input[name=InvestigationType]:checked').val() === '21') {
    //    alert('1');
    //    if ($("#NameOfForeignInvestigator").val() === '') {
    //        alert('2');
    //        TabIndex = 6;
    //        return { msg: "Enter Name Of Foreign Investigator", code: false };
    //    }
    //}
    //else {
    //    alert('3');
    //    return { msg: "", code: true };
    //}
}

function saveInvestigation(element, tabIndex) {
    $("#InvTabIndex").val(tabIndex);
    var ltm = changeDateFormat($("#LocalDateAndTimeOfOccurrence").val());
    $("#LocalDateAndTimeOfOccurrence").val(ltm);
    var ncd = changeDateFormat($("#NotifierCallDate").val());
    $("#NotifierCallDate").val(ncd);
    var udto = changeDateFormat($("#UTCDateAndTimeOfOccurrence").val());
    $("#UTCDateAndTimeOfOccurrence").val(udto);
    $(".btn-SaveGn,#Btnform35submit").hide();
    $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request.</div>');
    $('.alert-info').show();
    $("#" + element).submit();
    //setTimeout(function () { $("#" + element).submit() }, 1000);    
    ////$("#" + element).submit();


}

function SaveInvestigatioScope(element) {
    $("#" + element).submit();
}

function GetAllAttachments(fileElementId, hdnVal, SaveFileElement, tabIndex) {
    saveInvestigation(SaveFileElement, tabIndex);
    //var count = $('.' + fileElementId).length;
    //var file_Info = new Array(count);
    //var formdata = new FormData();
    //for (var i = 0; i < count; ++i) {
    //    file_Info[i] = new Array(5);
    //    var inp = $('.' + fileElementId)[i];
    //    var hdn = $('.' + hdnVal)[i];
    //    for (var j = 0; j < inp.files.length; j++) {
    //        file_Info[i][0] = inp.files[j].name;
    //        var f = inp.files[j];
    //        formdata.append(inp.files[j].name, inp.files[j]);
    //    }
    //}  
    //var xhr = new XMLHttpRequest();
    //xhr.open('POST', '/DI/SaveFiles');
    //xhr.send(formdata);  
}

function validateEvidence() {
    if ($("#LKEvidenceCategoryID").val() == '') {
        return { msg: "Select Evidence Category", code: false };
    }
    else if ($("#LKEvidenceTypeID").val() == '') {
        return { msg: "Select Evidence Type", code: false };
    }
    else if ($("#EvidenceName").val() == '') {
        return { msg: "Enter Evidence Name", code: false };
    }
    else if ($("#OrganizationHandingOverEvidence").val() == '') {
        return { msg: "Enter Organization Handing Over The Evidence", code: false };
    }
    else if ($("#DateOfReceipt").val() == '') {
        return { msg: "Select Date Of Receipt ", code: false };
    }
    else if ($("#PersonReceivingEvidence").val() == '') {
        return { msg: "Enter Evidence Received By", code: false };
    }
    else if ($("#EvidenceCurrentlyWith").val() == '') {
        return { msg: "Select Evidence Currently With", code: false };
    }
    else { return { msg: "", code: true }; }
}

function SaveEvidence() {
    data = validateEvidence();
    if (data.code == true) {
        var DateOf_Receipt = changeDateOnlyFormat($("#DateOfReceipt").val());
        $("#DateOfReceipt").val(DateOf_Receipt);
        $("#frm_Evidence").submit();
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
}

function validateMovement() {
    if ($("#EvidenceMovementType").val() == '') {
        return { msg: "Select Evidence Movement Type", code: false };
    }
    else if ($("#DateOfMovement").val() == '') {
        return { msg: "Select Date Of Movement", code: false };
    }
    else if ($("#MPersonReceivingEvidence").val() == '') {
        return { msg: "Enter Person Receiving Evidence", code: false };
    }
    else if ($("#OrganizationReceivingEvidence").val() == '') {
        return { msg: "Enter Department Receiving Evidence", code: false };
    }
    else if ($("#MPersonHandingOverEvidence").val() == '') {
        return { msg: "Enter Person Handing Over Evidence ", code: false };
    }
    else if ($("#MOrganizationHandingOverEvidence").val() == '') {
        return { msg: "Enter Department Sent By", code: false };
    }
    else if ($("#MCustodianOfEvidence").val() == '') {
        return { msg: "Enter Custodian Of Evidence", code: false };
    }
    else { return { msg: "", code: true }; }
}

function SaveMovement() {
    var Checkedstatus = $("#EvidenceReturned").is(":checked");
    if (Checkedstatus == true) {
        Checkedstatus = 1;
    } else {
        Checkedstatus = 0;
    }
    $("#EvidenceReturned").val(Checkedstatus);
    data = validateMovement();
    if (data.code == true) {
        var DateOf_Movement = changeDateOnlyFormat($("#DateOfMovement").val());
        $("#DateOfMovement").val(DateOf_Movement);
        $("#frm_Movement").submit();
    }
    else {
        //   $("#Eve_Movements").modal('hide');
        $('.popup_danger').html(data.msg);
        $('.popup_danger').show();
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
    }
}

function validateDistributionList() {
    if ($("#EmailDistributionListName").val() == '') {
        return { msg: "Enter Distribution List Name", code: false };
    }
    else if ($("#Recipients_Info").val() == '') {
        return { msg: "Enter Recipients Email ", code: false };
    }
    else { return { msg: "", code: true }; }
}

function SaveDistributionList() {
    data = validateDistributionList();
    if (data.code == true) {
        var Recipients_Info = $("#Recipients_Info").val();
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Task/ValidateRecipienceEmail',
            dataType: 'json',
            data: { RecEmail: Recipients_Info },
            error: function (err) {
            },
            success: function (result) {
                if (result == 1) {
                    $("#frm_distributionlist").submit();
                } else {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Enter Valid Recipients Email.</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
            }
        });
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
}

function SaveGlobalDistributionList() {
    data = validateDistributionList();
    if (data.code == true) {
        var Recipients_Info = $("#Recipients_Info").val();
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Admin/ValidateGlobalRecipienceEmail',
            dataType: 'json',
            data: { RecEmail: Recipients_Info },
            error: function (err) {
            },
            success: function (result) {
                if (result == 1) {
                    $("#frm_Globaldistributionlist").submit();
                } else {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Enter Valid Recipients Email.</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
            }
        });
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
}

function EvidenceTypeChange() {
    $("#FrmEvidence").submit();
}

function SaveAircraftRegistration() {
    var AircraftRegistrationID = $("#LKAircraftRegistrationID").val(); var CheckOperatorID = $("#LKAirlineOperatorID").val(); var CheckRegname = $("#LKAircraftRegistrationName").val();
    var CheckRegDesc = $("#LKAircraftRegistrationDescription").val(); var AirmodelId = $("#LKAircraftModelID").val(); var air_msn = $("#MSN").val();
    var Maximum_Mass = $("#MaximumMass").val(); var Engine_Info = $("#EngineInfo").val(); var Aircraft_CategoryId = $("#LKAircraftCategoryID").val();
    var Stateof_Registry = $("#StateofRegistry").val(); var Stateof_Operator = $("#StateofOperator").val(); var Operator_Contacts = $("#OperatorContacts").val();
    var Created_By = $("#CreatedBy").val(); var StateofRegistry_Id = $("#StateofRegistryID").val(); var StateofOperator_Id = $("#StateofOperatorID").val();

    var AirlineOperatorName = $("#LKAirlineOperatorName").val(); var AircraftModelName = $("#LKAircraftModelName").val(); var AircraftCategoryName = $("#LKAircraftCategoryName").val();

    if (CheckRegname == '') {
        $('.popup_danger').html('Please Enter Aircraft Registration');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else if (CheckOperatorID == '') {
        $('.popup_danger').html('Please Select Airline Operator Name');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/DI/AddAircraftRegistrations',
            dataType: 'json',
            data: {
                LKAircraftRegistrationID: parseInt(AircraftRegistrationID),
                LKAirlineOperatorID: parseInt(CheckOperatorID),
                LKAircraftRegistrationName: (CheckRegname),
                LKAircraftRegistrationDescription: (CheckRegDesc),
                LKAircraftModelID: AirmodelId,
                EngineInfo: Engine_Info
                , MaximumMass: Maximum_Mass
                , LKAircraftCategoryID: parseInt(Aircraft_CategoryId)
                , StateofRegistry: Stateof_Registry
                , StateofOperator: Stateof_Operator
                , OperatorContacts: Operator_Contacts
                , CreatedBy: Created_By
                , MSN: air_msn
                , LKAirlineOperatorName: AirlineOperatorName
                , LKAircraftModelName: AircraftModelName
                , LKAircraftCategoryName: AircraftCategoryName
                , StateofRegistryID: StateofRegistry_Id
                , StateofOperatorID: StateofOperator_Id
            },
            error: function (err) {
            },
            success: function (result) {
                if (result != 0) {
                    getNewregistrationValues($("#aircid").val(), result);
                    $("#Add_AircraftRegistration").modal('hide');
                }
                else {
                    $('.popup_danger').html('Aircraft Registration Already Exists');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                    return false;
                }
            }
        });
    }
}

function getNewregistrationValues(element, reg_id) {

    $.ajax
        ({
            url: '/Notifications/getregistrationValues',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#LKAircraftRegistrationID_" + element).html('');
                $("#LKAircraftRegistrationID_" + element).html($('<option></option>').val(0).html('Select'));
                $.each(JSON.parse(result), function (key, item) {
                    $("#LKAircraftRegistrationID_" + element).append
                        ($('<option></option>').val(item.LKAircraftRegistrationID).html(item.LKAircraftRegistrationName));

                    $("#addAircraftRegistration_" + element).html('Add Registration');

                    $("#LKAirlineOperatorID_" + element).html('');

                    $("#LKAircraftModelID_" + element).html('');

                    $("#AircraftCategoryId_" + element).html('');

                    $("#EngineInfo_" + element).val('');

                    $("#MaximumMass_" + element).val('');

                    $("#StateofRegistry_" + element).val('');

                    $("#stateofoperatorid_" + element).val('');

                    $("#OperatorContacts_" + element).val('');

                    $("#msn_" + element).val('');

                });

                $("#LKAircraftRegistrationID_" + element).val(reg_id);
                getNewAircraftDetailsByRegistrationId(reg_id, element);
            },
            error: function () {
                alert("Something went wrong..");
            },
        });
}

function getNewAircraftDetailsByRegistrationId(result, control_id) {
    var ctrlId = control_id;
    var RegId = result;
    $.ajax
        ({
            url: '/Notifications/getAircraftDetailsByRegistrationId',
            data: { RegId: parseInt(RegId) },
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {

                $("#LKAirlineOperatorID_" + ctrlId).html('');
                $("#LKAircraftModelID_" + ctrlId).html('');
                $("#AircraftCategoryId_" + ctrlId).html('');

                $.each(JSON.parse(result), function (key, item) {

                    if ($("#LKAircraftRegistrationID_" + ctrlId).val() > 0) {

                        $("#addAircraftRegistration_" + ctrlId).html('Edit Registration');
                    }
                    else {
                        $("#addAircraftRegistration_" + ctrlId).html('Add Registration')
                    }

                    $("#LKAirlineOperatorID_" + ctrlId).append
                        ($('<option></option>').val(item.LKAirlineOperatorID).html(item.LKAirlineOperatorName));

                    $("#LKAircraftModelID_" + ctrlId).append
                        ($('<option></option>').val(item.LKAircraftModelID).html(item.LKAircraftModelName));

                    $("#AircraftCategoryId_" + ctrlId).append
                        ($('<option></option>').val(item.LKAircraftCategoryID).html(item.LKAircraftCategoryName));

                    $("#EngineInfo_" + ctrlId).val(item.EngineInfo);

                    $("#MaximumMass_" + ctrlId).val(item.MaximumMass);

                    $("#StateofRegistry_" + ctrlId).val(item.StateofRegistry);

                    $("#stateofoperatorid_" + ctrlId).val(item.StateofOperator);

                    //$("#OperatorContacts_" + ctrlId).val(item.OperatorContacts);

                    $("#msn_" + ctrlId).val(item.MSN);
                });
            },
            error: function () {

            },
        });
}

function SaveAirlineOperatorName() {

    var CheckOpername = $("#LKAirlineOperatorName").val();

    var CheckOperDesc = $("#LKAirlineOperatorDescription").val();

    if (CheckOpername == '') {
        $('.popup_danger').html('Please Enter Airline Operator Name');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/DI/AddAirOperatorName',
            dataType: 'json',
            data: { LKAirlineOperatorName: (CheckOpername), LKAirlineOperatorDescription: (CheckOperDesc) },
            error: function (err) {

            },
            success: function (result) {

                if (result == 1) {
                    $("#Add_AirlineOperator").modal('hide');
                }
                else {
                    $('.popup_danger').html('Airline Operator Name Already Exists');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                    return false;
                }
            }
        });
    }
}

function SaveUserDesignation() {

    var CheckDesignId = $("#LKDesignationID").val(); var CheckDesignName = $("#LKDesignationName").val();

    if (CheckDesignName == '') {
        $('.popup_danger').html('Please Enter User Designation');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/Admin/AddDesignations',
            dataType: 'json',
            data: { LKDesignationID: (CheckDesignId), LKDesignationName: (CheckDesignName) },
            error: function (err) {

            },
            success: function (result) {

                if (result == 1) {
                    $("#Add_UserDesignation").modal('hide');
                    window.location.reload();
                }
                else {
                    $('.popup_danger').html('Designation Already Exists');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                    return false;
                }
            }
        });
    }
}

function UpdateIncidentType() {

    var IncidentTypeID = $("#LKIncidentTypeID:checked").val();

    var Investigation_FileID = $("#InvestigationFileID").val();

    var Inv_otherdescr = $("#OtherOccurClassification").val();

    if ($('input[name=LKIncidentTypeID]:checked').val() === "11" && $("#OtherOccurClassification").val() === '') {
        $('.popup_danger').html('Please Enter Other Occurrence Details');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/DI/ChangeIncidentTypeSave',
            dataType: 'json',
            data: { LKIncidentTypeID: parseInt(IncidentTypeID), InvestigationFileID: parseInt(Investigation_FileID), OtherOccType: Inv_otherdescr },
            error: function (err) {

            },
            success: function (result) {

                if (result == 1) {
                    $("#Change_IncType").modal('hide');
                    window.location.reload();
                }
                else {
                    //$('.popup_danger').html('Airline Operator Name Already Exists');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                    return false;
                }
            }
        });
    }
}

function SendCorrEmail() {

    var SRID = $("#SR_Number").val();

    var Email = $("#CorrespondenceMessageRecipientEmailAddress").val();

    var CORRID = $("#CorrespondenceID").val();

    if ($('#CorrespondenceMessageRecipientEmailAddress').val() ==='') {
        $('.popup_danger').html('Please Enter valid Email Address');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/IIC/ResendEmailUpdates',
            dataType: 'json',
            data: { SrId: SRID, CorrId: parseInt(CORRID), EmailId: Email},
            error: function (err) {

            },
            success: function (result) {

                if (result === 1) {
                    $("#ResendEmail").modal('hide');
                    window.location.reload();
                }
                else {
                    //$('.popup_danger').html('Airline Operator Name Already Exists');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                    return false;
                }
            }
        });
    }
}

function SaveEvidenceWithAttachments(fileElementId, hdnVal, SaveFileElement) {

    SaveEvidence(SaveFileElement);

    //var count = $('.' + fileElementId).length;
    //var file_Info = new Array(count);
    //var formdata = new FormData();
    //for (var i = 0; i < count; ++i) {
    //    file_Info[i] = new Array(5);
    //    var inp = $('.' + fileElementId)[i];
    //    var hdn = $('.' + hdnVal)[i];
    //    for (var j = 0; j < inp.files.length; j++) {
    //        file_Info[i][0] = inp.files[j].name;
    //        var f = inp.files[j];
    //        formdata.append(inp.files[j].name, inp.files[j]);
    //    }
    //}
    //var xhr = new XMLHttpRequest();
    //xhr.open('POST', '/Task/SaveEvidenceFiles');
    //xhr.send(formdata);
}

$("#Recipients_Info").tagsinput({
    confirmKeys: [13, 32, 44]
});

$("#EvidenceTags").tagsinput({
    confirmKeys: [13, 32, 44]
});

function SaveCloseInvestigation() {

    var CheckClosureReason = $("#InvestigationClosureReason").val();

    if (CheckClosureReason != '') {

        $("#save_closure").hide();
        $('.popup_info').html('Processing request, please wait');
        $(".popup_info").css({ display: "block" });
        $("#frm_CloseInvestigation").submit();

    }
    else {
        $('.popup_danger').html('Please enter Investigation Closure Reason');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
}
function SaveReopenInvestigation() {

    var CheckClosureReason = $("#InvestigationReopenReason").val();
    debugger
    if (CheckClosureReason != '') {

        $("#save_reopen").hide();
        $('.popup_info').html('Processing request, please wait');
        $(".popup_info").css({ display: "block" });
        $("#frm_ReopenCloseInvestigation").submit();
    }
    else {
        $('.popup_danger').html('Please enter Investigation Closure Reason');
        $(".popup_danger").css({ display: "block" });
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
}

function UpdateCloseInvestigation() {

    var ClosureStatusId = $("#InvestigationClosureStatusId").val();

    if (ClosureStatusId != '') {
        $("#frm_UpdateCloseInvestigation").submit();
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + "Please Select Investigation Clouser Status." + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
}

function SetTheme() {
    $("#frm_Theme").submit();
}
var Fi = 1;
var Ffileupload = true;
$("#occ_add_attachment").click(function (e) {
    var F35controlcount = $('.Form35-upload').length;
    for (var ii = 0; ii < F35controlcount; ii++) {
        var Finp = $('.Form35-upload')[ii];
        if (Finp.files.length == 0) {
            Ffileupload = false;
            break;
        }
        else
            Ffileupload = true;
    }
    if (Ffileupload == false) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
    else {
        //var html = '<div class="col-sm-12 col-form-box no-padding" style="margin-bottom: 15px;"><div class="input-group" ><input id="uploadFile_' + Fi + '" class="form-control" placeholder= "Maximum file size limited to 100MB" disabled= "disabled" > <div class="input-group-btn"><div class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="uploadBtn_' + Fi + '" type="file" InvestigationFiles[] class="upload Form35-upload" required="required"><input type="hidden" id="hdnVal_' + Fi + '" class="hdnfile"/></div > <span> <a id="RemoveAttach_'+ Fi +'" class="remove_Fileattachment" onclick=removesFiles(this)>Remove</a></span></div ></div ></div> ';      
        var html = ' <div class="input-group tag-bottom"><input id="uploadFile_' + Fi + '" class="form-control" placeholder="Maximum file size limited to 100MB"><div class="input-group-btn"><div id="FileUpload_' + Fi + '" class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="uploadBtn_' + Fi + '" type="file" name="InvestigationFiles[]" class="upload Form35-upload" required="required"><input type="hidden" id="hdnVal_' + Fi + '" class="hdnfile"></div><a href="javascript:void(0);" class="remove_Attached_file" onclick=removesFiles(this) id="RemoveAttach_' + Fi + '"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>';
        //<div><a id="RemoveAttach_' + Fi +'" class="remove_Fileattachment" onclick=removesFiles(this)>Remove</a></div>
        $(".attachments-tag").append(html);
        Fi++;
    }
});

function removesFiles(e) {
    var attach_ids = $(e).attr('id').split('_');
    var attach_name = $('#uploadFile_' + parseInt(attach_ids[1])).val();
    //var attachurl = $(this).prev().attr('href');
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/DI/RemoveAttachedFile',
        dataType: 'json',
        data: { filename: (attach_name) },
        error: function (err) {

        },
        success: function (result) {
            $('#uploadFile_' + parseInt(attach_ids[1])).remove();
            $('#uploadBtn_' + parseInt(attach_ids[1])).remove();
            $('#RemoveAttach_' + parseInt(attach_ids[1])).remove();
            $('#FileUpload_' + parseInt(attach_ids[1])).remove();
        }
    });
}

var Ei = 1;
var Efileupload = true;

function removesEvidenveFiles(e) {
    var attach_ids = $(e).attr('id').split('_');
    var attach_name = $('#uploadFile_' + parseInt(attach_ids[1])).val();
    //var attachurl = $(this).prev().attr('href');
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/Task/RemoveEvidenceAttachedFile',
        dataType: 'json',
        data: { filename: (attach_name) },
        error: function (err) {

        },
        success: function (result) {
            $('#uploadFile_' + parseInt(attach_ids[1])).remove();
            $('#uploadBtn_' + parseInt(attach_ids[1])).remove();
            $('#RemoveAttach_' + parseInt(attach_ids[1])).remove();
            $('#FileUpload_' + parseInt(attach_ids[1])).remove();
        }
    });
}

$("#evi_add_attachment").click(function (e) {
    var Evcontrolcount = $('.EvidenceFileUpload').length;
    for (var iii = 0; iii < Evcontrolcount; iii++) {
        var Einp = $('.EvidenceFileUpload')[iii];
        if (Einp.files.length == 0) {
            Efileupload = false;
            break;
        }
        else
            Efileupload = true;
    }
    if (Efileupload == false) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
    else {
        //var html = '<div class="col-sm-12 col-form-box no-padding" style="margin-bottom: 15px;"><div class="input-group" ><input id="uploadFile_' + Ei + '" class="form-control" placeholder= "Maximum file size limited to 100MB" disabled= "disabled" > <div class="input-group-btn"><div class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="uploadBtn_' + Ei + '" type="file" name="EvidenceFiles[]" class="upload EvidenceFileUpload" required="required"><input type="hidden" id="hdnVal_' + Ei + '" class="hdnfile"/></div></div ></div></div> ';
        var html = ' <div class="input-group tag-bottom"><input id="uploadFile_' + Ei + '" class="form-control" placeholder="Maximum file size limited to 100MB"><div class="input-group-btn"><div id="FileUpload_' + Ei + '" class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="uploadBtn_' + Ei + '" type="file" name="EvidenceFiles[]" class="upload EvidenceFileUpload EveFileupload" required="required"><input type="hidden" id="hdnVal_' + Ei + '" class="hdnfile"></div><a href="javascript:void(0);" class="remove_Attached_file" onclick=removesEvidenveFiles(this) id="RemoveAttach_' + Ei + '"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>';
        //<div><a id="RemoveAttach_' + Ei + '" class="remove_Fileattachment" onclick=removesEvidenveFiles(this)>Remove</a></div>
        $(".attachments-tag").append(html);
        Ei++;
    }
});

function getSRNumberBySRTpe(e) {

    var RegId = e.value;

    if (RegId === "") { RegId = parseInt(0); }

    var sr_year = $("#SR_Date").val();

    $.ajax
        ({
            url: '/SR/getSRNumber',
            data: { id: parseInt($("#InvestigationFileID").val()), srtype: parseInt(RegId), sryear: parseInt(sr_year) },
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#SafetyRecommendationNumber").val(result);
                $("#SafetyRecommendationNumber").html(result);
                $("#SafetyRecommendation_Number").val(result);
                $("#SafetyRecommendation_Number").html(result);
            },
            error: function () {

            },
        });
}

function FilterDIDashboard() {

    //$("#DIForm").submit();

    //$.ajax
    //    ({
    //        url: '/Dashboard/DIDashboard',
    //        data: { page: parseInt($("#page").val()), occuryear: parseInt($("#Occurrenceyear").val()), occstatusid: parseInt($("#LKOccurrenceStatusTypeId").val())},
    //        type: 'GET',
    //        datatype: 'application/json',
    //        contentType: 'application/json',
    //        success: function () {                
    //            //location.reload();
    //        },
    //        error: function () {

    //        },
    //    });   
}
var doNotUse_Words = [
    "known as",
    "called",
    "so called",
    "etc",
    "Period",
    "TACAN",
    "radar",
    "i.e",
    "et al",
    "so on",
    "e.g",     
    "purser",
    "steward",
    "air hostess",
    "- nationwide",
    "- systemwide",
    "- lengthwise",
    "- wirelike",
    "- industrywide",
    "- speechless",
    "‘Ltd’",
    "‘known as’",
    "‘so called’",
    "Vic"
];

function chkreportText() {
    var ret_val = true;
    if (($('.reports').val()) !== null) {
        $('.reports').each(function () {
            var fieid_val = this.value.trim();
            if (fieid_val !== "") {
                for (var wo = 0; wo < doNotUse_Words.length; wo++) {
                    if (fieid_val.indexOf(doNotUse_Words[wo]) >= 0) {
                        ret_val = false;
                        $(this).css("border-color", "Red");
                        this.focus();
                        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>The word " ' + doNotUse_Words[wo] + ' " are not used in AAIS reports. Please remove it  before saving or press "Yes" and save it again ..</div>');
                        $('.alert-danger').show();                       
                        $('#EventConfirm-dialog').dialog('open');
                        setTimeout(function () { $(".alert").hide(); }, 3000);                     
                        break;
                    }
                    else {
                        $('.alert-danger').hide();
                        ret_val = true;
                    }
                }
            }
            if (!ret_val) {
                return ret_val;
            }
        });
    }
    return ret_val;
}
