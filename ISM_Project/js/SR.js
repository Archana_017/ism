﻿function validateSRForm() {
    if ((tinyMCE.get('SafetyRecommendation')) != null) {
        var SR = (tinyMCE.get('SafetyRecommendation').getContent());
        if (SR == '' || SR == null) {            
            return { msg: "Please enter the safety recommendation", code: false };
        }
    }
    if ($("#DAAIStatus").val() != undefined) {
        if ($("#DAAIStatus").val() == 'N/A') {
            return { msg: "Please select status", code: false };
        }
    }
    if ($("#DateChanged").val() != undefined && $("#DateChanged").val() == '') {
        return { msg: "Please select updated date", code: false };
    }
    if ($("#ReasonForUpdate").val() != undefined && $("#ReasonForUpdate").val() == '') {
        return { msg: "Please select reason for update", code: false };
    }
    if ( $("#SafetyRecommendationNumber").val() == '') {
        return { msg: "Please enter Safety Recommendation Number", code: false };
    }
    else {
        return { msg: "", code: true };
    }
}
function validateSR() {
    if ((tinyMCE.get('SafetyRecommendation')) != null) {
        var SR = (tinyMCE.get('SafetyRecommendation').getContent());
        if (SR == '' || SR == null) {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter the safety recommendation</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
        }
        else {
            $('#IICUpdate').attr('data-target', '#incharge-popup');
            $('#IICUpdate').attr('data-toggle', 'modal');
        }
    }
    else {
        $('#IICUpdate').attr('data-target', '#incharge-popup');
        $('#IICUpdate').attr('data-toggle', 'modal');
    }
}
$('#btnResponseNotReceived').click(function () {
    var SRID = $(this).attr('data-id');
    $(this).hide();
    $.ajax({
        url: '/SR/getSRForNoResponse',
        method: 'POST',
        datatype: 'application/json',
        data: { SRID: parseInt(SRID) },
        success: function (result) {
            $('#divbtnIICcommentsForSHNoResponse').show();
            var ResultJson = JSON.parse(result);
            $('#SRResponseID').val(ResultJson.SRResponseID);
            $('#SRHistoryID').val('');
            $('#StakeholderResponse').attr("disabled", true);
            $('#StakeholderResponse').text('');
            $("#stk-attach").hide();
            $('#IICCommentsforSHResponse').text('');
            $('#IICCommentsforSHResponse').attr("disabled", false);
            $('#SRIMPLEMENTATIONSTATUSLbl').hide();
            $('#IICImplementaionStatus').show();
            $('#divSRCommitteeComments').hide();
            $('#divSRCommitteeClosureStatus').hide();
            $('#divIICClosureComments').hide();
            $('#divbtnIICcommentsForSH').hide();
            $('#divbtnIICcommentsForSRCommittee').hide();
            $("#AddSRCommun").show();
        }
    });
});
$('.showRes').click(function () {
    // var ResponseId = e.id.split('_')[1];
    var ResponseId = $(this).attr('id').split('_')[1];
    var HistoryId = $(this).attr('data-history');
    $.ajax({
        url: '/SR/getSRResponse',
        method: 'POST',
        datatype: 'application/json',
        data: { SRHistoryID: parseInt(HistoryId) },
        success: function (result) {
            var ResponseJson = JSON.parse(result);
            //var resp = $.map(ResponseJson, function (obj) {
            $('#StakeholderResponse').text(ResponseJson.StakeholderResponse);
            $('#StakeholderResponse').attr("disabled", true);
            $('#SRHistoryID').val(ResponseJson.SRHistoryID);
            $('#SRResponseID').val(ResponseId);
            $('#SRStatus').val(ResponseJson.SRStatus);
            $.ajax({
                url: '/SR/getSRResponseAttachments',
                method: 'POST',
                datatype: 'application/json',
                data: { SRID: parseInt(ResponseJson.SRID), SRResponseID: parseInt(ResponseId) },
                success: function (Attachresult) {
                    if (Attachresult == "[]") {
                        $("#stk-attach").hide();
                    }
                    else {
                        var AttachJson = JSON.parse(Attachresult);
                        $("#ulStkAttach").html('');
                        $.each(AttachJson, function (key, value) {
                            if (value.AttachmentPublicUrl != null || value.AttachmentPublicUrl != "" || value.AttachmentPublicUrl != undefined) {
                                $("#ulStkAttach").append("<li id='list_'" + value.InvestigationSafetyRecommendationAttachmentID + "'><a href = '" + value.AttachmentAnonymousUrl + "' id = 'file_'" + value.InvestigationSafetyRecommendationAttachmentID + "' target = '_blank'>" + value.AttachmentPublicUrl.slice(value.AttachmentPublicUrl.lastIndexOf('/') + 1) + "</a></li>");
                            }

                        });
                        $("#stk-attach").show();
                    }
                }
            });
            if (ResponseJson.SRStatus == 6 || ResponseJson.SRStatus == 7) {
                $('#IICCommentsforSHResponse').text(ResponseJson.IICCommentsforSHResponse);
                $('#IICCommentsforSHResponse').attr("disabled", true);
                $('#SRIMPLEMENTATIONSTATUSLbl').text(ResponseJson.SRIMPLEMENTATIONSTATUS);
                $('#SRIMPLEMENTATIONSTATUS').val(ResponseJson.SRIMPLEMENTATIONSTATUS);
                $('#SRIMPLEMENTATIONSTATUSLbl').show();
                $('#IICImplementaionStatus').hide();
                $('#SRCommitteeComments').text(ResponseJson.SRCommitteeComments);
                $('#SRCommitteeComments').attr("disabled", true);
                $('#SRCommitteeClosureStatus').text(ResponseJson.SRCommitteeClosureStatus);
                $('#divSRCommitteeComments').show();
                $('#divSRCommitteeClosureStatus').show();
                $('#divIICClosureComments').show();
                $('#divbtnIICcommentsForSH').hide();
                $('#divbtnIICcommentsForSHNoResponse').hide();
                $('#divbtnIICcommentsForSRCommittee').show();
                if (ResponseJson.SRStatus == 7) {
                    $('#divbtnIICcommentsForSRCommittee').hide();
                    $('#divIICClosureComments').hide();
                }
                $('#btnResponseNotReceived').show();
            }
            else if (ResponseJson.SRStatus == 3) {
                $('#IICCommentsforSHResponse').text('');
                $('#IICCommentsforSHResponse').attr("disabled", false);
                $('#SRIMPLEMENTATIONSTATUSLbl').hide();
                $('#IICImplementaionStatus').show();
                $('#divSRCommitteeComments').hide();
                $('#divSRCommitteeClosureStatus').hide();
                $('#divIICClosureComments').hide();
                $('#divbtnIICcommentsForSH').show();
                $('#divbtnIICcommentsForSHNoResponse').hide();
                $('#divbtnIICcommentsForSRCommittee').hide();
                $('#btnResponseNotReceived').show();

            }
            else if (ResponseJson.SRStatus == 5 || ResponseJson.SRStatus == 4) {
                $('#IICCommentsforSHResponse').text(ResponseJson.IICCommentsforSHResponse);
                $('#IICCommentsforSHResponse').attr("disabled", true);
                $('#SRIMPLEMENTATIONSTATUSLbl').text(ResponseJson.SRIMPLEMENTATIONSTATUS);
                $('#SRIMPLEMENTATIONSTATUSLbl').show();
                $('#divSRCommitteeComments').hide();
                $('#divSRCommitteeClosureStatus').hide();
                $('#IICImplementaionStatus').hide();
                $('#divbtnIICcommentsForSH').hide();
                $('#divbtnIICcommentsForSHNoResponse').hide();
                $('#divbtnIICcommentsForSRCommittee').hide();
                $('#btnResponseNotReceived').show();

            }
            else if (ResponseJson.SRStatus == 8) {
                $('#IICCommentsforSHResponse').text(ResponseJson.IICCommentsforSHResponse);
                $('#IICCommentsforSHResponse').attr("disabled", true);
                $('#SRIMPLEMENTATIONSTATUSLbl').text(ResponseJson.SRIMPLEMENTATIONSTATUS);
                $('#SRIMPLEMENTATIONSTATUSLbl').show();
                $('#IICImplementaionStatus').hide();
                $('#divbtnIICcommentsForSH').hide();
                $('#SRCommitteeComments').text(ResponseJson.SRCommitteeComments);
                $('#SRCommitteeComments').attr("disabled", true);
                $('#SRCommitteeClosureStatus').text(ResponseJson.SRCommitteeClosureStatus);
                $('#divSRCommitteeComments').show();
                $('#divSRCommitteeClosureStatus').show();
                $('#divIICClosureComments').show();
                $('#divbtnIICcommentsForSHNoResponse').hide();
                $('#IICClosureComments').text(ResponseJson.IICClosureComments);
                $('#IICClosureComments').attr("disabled", true);
                $('#divbtnIICcommentsForSRCommittee').hide();
                $('#btnResponseNotReceived').show();

            }
            $("#AddSRCommun").show();
        }
    });
});
$("#Existing_SR").on("click", ":button", function (e) {
    var cmdAction = $(this).val();
    isValid = validSR(cmdAction);
    if (isValid.code == true) {
        $("#SRCommandAction").val($(this).val());
        $('#btnsaveIICActionforSH').hide();
        $('#btnsaveIICActionforSH').hide();
        $('#btnRejectSRResponse').hide();
        $('#btnsaveIICActionforSRC').hide();
        $("#Existing_SR").submit();
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + isValid.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
});

function validSR(cmd) {
    if ($('#IICCommentsforSHResponse').attr("disabled", false) && $('#IICCommentsforSHResponse').val() == "") {
        $('#IICCommentsforSHResponse').focus();
        return { msg: "Please enter comments", code: false };
    }

    //if (($('#IICImplementaionStatus').is(':visible')) && $('#IICImplementaionStatus').val() == "" && cmd == 'SenttoCommittee') {
    //    $('#IICImplementaionStatus').focus();
    //    return { msg: "Please select implementaion status", code: false };
    //}
    if (($('#IICClosureComments').is(':visible')) && $('#IICClosureComments').val() == "") {
        $('#IICClosureComments').focus();
        return { msg: "Please enter closure comments", code: false };
    }
    else {
        return { msg: "", code: true };
    }
}
//function updateSRFiles() {
//    //    var sr_no = document.getElementById('SR_NO').value;
//    //    //$.ajax({
//    //    //    type: "POST",
//    //    //    cache: false,
//    //    //    url: '/SR/UpdateFiles',
//    //    //    dataType: 'json',
//    //    //    data: { srno: sr_no },
//    //    //    error: function (err) {

//    //    //    },
//    //    //    success: function (result) {
//    //    //        console.log(result);
//    //    //        window.location.reload();
//    //    //    }
//    //    //});

//    $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
//    $('.alert-info').show();
//    $("#btnupdateSR").hide();
//    $("#new_SR").submit();
//    setTimeout(function () { $(".alert").hide(); }, 8000);
//}

function saveNewSR() {

    data = validateSRForm();
   
    if ($('input[type="hidden"][name="SafetyRecommendation"]').val() != undefined) {
        $('input[type="hidden"][name="SafetyRecommendation"]').val((tinyMCE.get('SafetyRecommendation').getContent()));
    }

    if (data.code == true) {       
        
        var sr_assignTo = $("#SafetyRecommendationAssignedTo").val();

        if (sr_assignTo == '' || sr_assignTo == null) {
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter the safety recommendation Assigned To </div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
            return false;
        }

        if ($("#SafetyRecommendationNumber").val() != '') {

            var sr_no = $("#SafetyRecommendationNumber").val();

            var sr_typeid = $("#TypeofsafetyRecommendation").val();

            $.ajax({
                url: '/SR/ValidateSR',
                method: 'POST',
                datatype: 'application/json',
                data: { Srno: (sr_no), Srid: parseInt(0), Srtypeid: parseInt(sr_typeid) },
                success: function (result) {
                    console.log(result);
                    if (result == 0) {
                        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter valid SR Number</div>');
                        $('.alert-danger').show();
                        setTimeout(function () { $(".alert").hide(); }, 2000);
                        return false;
                    }
                    else if (result == 2) {
                        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>SR Number Already Exist</div>');
                        $('.alert-danger').show();
                        setTimeout(function () { $(".alert").hide(); }, 2000);
                        return false;
                    }
                    else {
                        if ($('#DateChanged').val() != '' && $('#DateChanged').val() != undefined) {
                            var dateChanged = changeDateOnlyFormat($("#DateChanged").val());
                            $("#DateChanged").val(dateChanged);
                        }
                        $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
                        $('.alert-info').show();
                        $("#btnSaveSR").hide();
                        $("#new_SR").submit();
                        $("#incharge-popup").hide();
                        setTimeout(function () { $(".alert").hide(); }, 8000);

                        return { msg: "", code: true };
                    }
                }
            });
        }
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
}

function updateSRFiles() {

    var sr_assignTo = $("#SafetyRecommendationAssignedTo").val();

    if ($("#SafetyRecommendationNumber").val() == '') {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter valid SR Number</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
        return false;
    }  
    else if (sr_assignTo == '' || sr_assignTo == null) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter the safety recommendation Assigned To </div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
        return false;
    }
 
    if ($("#SafetyRecommendationNumber").val() != '') {

        var sr_no = $("#SafetyRecommendationNumber").val();

        var sr_typeid = $("#TypeofsafetyRecommendation").val();

        var Sr_id = $("#SRID").val();

        $.ajax({
            url: '/SR/ValidateSR',
            method: 'POST',
            datatype: 'application/json',
            data: { Srno: (sr_no), Srid: parseInt(Sr_id), Srtypeid: parseInt(sr_typeid) },
            success: function (result) {
                console.log(result);
                if (result == 0) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter valid SR Number</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                    return false;
                }
                else if (result == 2) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>SR Number Already Exist</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                    return false;
                }
                else {
                    if ($('#DateChanged').val() != '' && $('#DateChanged').val() != undefined) {
                        var dateChanged = changeDateOnlyFormat($("#DateChanged").val());
                        $("#DateChanged").val(dateChanged);
                    }
                    $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
                    $('.alert-info').show();
                    $("#btnupdateSR").hide();
                    $("#new_SR").submit();
                    $("#incharge-popup").hide();
                    setTimeout(function () { $(".alert").hide(); }, 8000);

                    return { msg: "", code: true };
                }
            }
        });
    }   
}

function removeSRFiles(e) {
    var attach_ids = $(e).attr('id').split('_');
    var attach_name = $('#SRuploadFile_' + parseInt(attach_ids[1])).val();
    //var attachurl = $(this).prev().attr('href');
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/SR/RemoveSRAttachedFile',
        dataType: 'json',
        data: { filename: (attach_name) },
        error: function (err) {

        },
        success: function (result) {
            $('#SRuploadFile_' + parseInt(attach_ids[1])).remove();
            $('#corresuploadBtn_' + parseInt(attach_ids[1])).remove();
            $('#RemoveAttach_' + parseInt(attach_ids[1])).remove();
            $('#FileUpload_' + parseInt(attach_ids[1])).remove();
        }
    });
}

$("#SR_add_attachment").click(function (e) {
    var controlcount = $('.SRupload').length;
    for (var ii = 0; ii < controlcount; ii++) {
        var inp = $('.SRupload')[ii];
        if (inp.files.length == 0) {
            fileupload = false;
            break;
        }
        else
            fileupload = true;
    }
    if (fileupload == false) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
    else {
        var html = '<div class="col-sm-12 col-form-box no-padding" style="margin-bottom: 15px;"><div class="input-group" ><input id="SRuploadFile_' + i + '" class="form-control" placeholder= "Choose File" disabled= "disabled" > <div class="input-group-btn"><div id="FileUpload_' + i + '" class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="SRuploadBtn_' + i + '" type="file" name="SRFiles[]" class="upload SRupload" required="required"><input type="hidden" id="SRhdnVal_' + i + '" class="hdnfile"/></div><a href="javascript:void(0);" class="remove_Attached_file" onclick=removeSRFiles(this) id="RemoveAttach_' + i + '"><i class="fa fa-times" aria-hidden="true"></i></a></div></div></div> ';
        //<div><a id="RemoveAttach_' + i + '" class="remove_SRFileattachment" onclick=removeSRFiles(this)>Remove</a></div>
        $(".attachments-tag").append(html);
        i++;
    }
});
$(document.body).on('change', '.SRupload', function (e) {
    var img = $(this).val();
    var num = $(this).attr('id').split('_');
    var file_number = num[1];
    $("#SRuploadFile_" + file_number).val(img);


    var files = e.target.files;

    if (files.length > 0) {
        if (window.FormData !== undefined) {
            var data = new FormData();
            for (var x = 0; x < files.length; x++) {
                data.append("file" + x, files[x]);
            }

            $.ajax({
                type: "POST",
                url: '/SR/UploadSRFile',
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    console.log(result);
                },
                error: function (xhr, status, p3, p4) {
                    var err = "Error " + " " + status + " " + p3 + " " + p4;
                    if (xhr.responseText && xhr.responseText[0] == "{")
                        err = JSON.parse(xhr.responseText).Message;
                    console.log(err);
                }
            });
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }


});
$(".remove_SRattachment").click(function (e) {
    e.preventDefault();
    var attach_ids = $(this).attr('id').split('_');
    var attach_id = attach_ids[1];
    var attachurl = $(this).prev().attr('href');
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/SR/DeleteAttachment',
        dataType: 'json',
        data: { id: parseInt(attach_id), path: attachurl },
        error: function (err) {

        },
        success: function (result) {

            $('#attach_' + attach_id).remove();
            $('#file_' + attach_id).remove();
            $('#list_' + attach_id).remove();
        }
    });
});

function searchSR() {
    $("#SRForm").submit();
}