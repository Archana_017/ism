﻿function validateForm() {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($("#HdnMode").val() == "New") {

        //if ($("#CorrespondenceControlNumber").val() == '') {
        //    $("#CorrespondenceControlNumber").focus();
        //    return { msg: "Enter correspondence control number", code: false };
        //}
        //else
        if ($("#CorrespondenceInitiatedDate").val() == '') {
            $("#CorrespondenceInitiatedDate").focus();
            return { msg: "Select correspondence initiated date", code: false };
        }
        if ($("#Subject").val() == '') {
            $("#Subject").focus();
            return { msg: "Enter subject", code: false };
        }
        if ($("#CorrespondenceMessageFrom").val() == '') {
            $("#CorrespondenceMessageFrom").focus();
            return { msg: "Enter correspondence message from mail address", code: false };
        }
        if ($("#CorrespondenceMessageFrom").val() != '') {
            var fromAddress = $("#CorrespondenceMessageFrom").val();
            if (!emailReg.test(fromAddress)) {
                $("#CorrespondenceMessageFrom").focus();
                return { msg: "Enter valid email address in correspondence message from", code: false };
            }
        }
        if ($("#LKCorrespondenceMessageFromRoleID").val() == '') {
            $("#LKCorrespondenceMessageFromRoleID").focus();
            return { msg: "Select role of the person", code: false };
        }
        if ($("#LKCorrespondenceMessageFromRoleID").val() == '7') {
            if ($('#CorrespondenceMessageOtherRoleName').val() == '') {
                $("#CorrespondenceMessageOtherRoleName").focus();
                return { msg: "Enter role of the person", code: false };
            }
        }
        if ($("#LKCorrespondenceMessageTypeID").val() == '') {
            $("#LKCorrespondenceMessageTypeID").focus();
            return { msg: "Select correspondence type", code: false };
        }
        if ($("#CorrespondenceDueDate").val() == 'N/A') {
            $("#CorrespondenceDueDate").focus();
            return { msg: "Select correspondence response due date", code: false };
        }
        if ($("#LKCorrespondenceMessageTypeID").val() == '7') {
            if ($('#LKCorrespondenceMessageOtherTypeName').val() == '') {
                $("#LKCorrespondenceMessageOtherTypeName").focus();
                return { msg: "Enter correspondence type", code: false };
            }
        }
        if ($("#LKCorrespondenceMessageTypeID").val() == '6') {
            if ($('#SR_Number').val() == '') {
                $("#SR_Number").focus();
                return { msg: "Enter SR Number", code: false };
            }
        }
        if ($("#DAAIApprovalRequired").val() == 'N/A') {
            $("#DAAIApprovalRequired").focus();
            return { msg: "Select DAAI approval required", code: false };
        }
        if ($("#CorrespondenceMessageRecipientEmailAddress").val() == '') {
            $("#CorrespondenceMessageRecipientEmailAddress").focus();
            return { msg: "Enter To recipients", code: false };
        }
        //if ($("#CorrespondenceMessageExtRecipientEmailAddress").val() == '') {
        //    $("#CorrespondenceMessageExtRecipientEmailAddress").focus();
        //    return { msg: "Enter CC recipients", code: false };
        //}
        //if ($("#CorrespondenceMessageExtRecipientEmailAddress").val() != '') {
        //    var extrecccount = 0;
        //    if ($('#CorrespondenceMessageExtRecipientEmailAddress').val().indexOf(',') > 0)
        //        extrecccount = $('#CorrespondenceMessageExtRecipientEmailAddress').val().split(',').length;
        //    else
        //        extrecccount = 1;
        //    for (var count = 0; count < extrecccount; count++) {
        //        var recedetail = $("#CorrespondenceMessageExtRecipientEmailAddress").tagsinput('items')[count];
        //        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        //        if (!emailReg.test(recedetail)) {
        //            $("#CorrespondenceMessageExtRecipientEmailAddress").focus();
        //            return { msg: "Enter valid email address in external recipients", code: false };
        //        }
        //    }
        //}
        if ($("#CorrespondenceTags").val() == '') {
            $("#CorrespondenceTags").focus();
            return { msg: "Enter correspondence tags", code: false };
        }
        if ($("#CorrespondenceContent").val() == '') {
            $("#CorrespondenceContent").focus();
            return { msg: "Enter correspondence content", code: false };
        }
        if ($("#LKCorrespondenceMessageTypeID").val() == '6') {
            if ($("#CorrespondenceMessageRecipientEmailAddress").val() != '') {
                if ($('#CorrespondenceMessageRecipientEmailAddress').val().indexOf(',') > 0) {
                    var reccount = $('#CorrespondenceMessageRecipientEmailAddress').val().split(',').length;
                    if (reccount == 2) {
                        var lastval = $('#CorrespondenceMessageRecipientEmailAddress').val().split(',')[reccount - 1];
                        if (lastval != " ") {
                            $("#CorrespondenceMessageRecipientEmailAddress").focus();
                            return { msg: "SR can be sent to only one recipient.So please enter only one recipient", code: false };
                        }
                        else {
                            return { msg: "", code: true };
                        }
                    }
                    else {
                        $("#CorrespondenceMessageRecipientEmailAddress").focus();
                        return { msg: "SR can be sent to only one recipient.So please enter only one recipient", code: false };
                    }
                }
                else {
                    var ToUser = $("#CorrespondenceMessageRecipientEmailAddress").val();
                    if (!emailReg.test(ToUser)) {
                        $("#CorrespondenceMessageRecipientEmailAddress").focus();
                        return { msg: "Enter valid email address in correspondence message from", code: false };
                    }
                    else {
                        return { msg: "", code: true };
                    }
                }
            }
        }
        else {
            return { msg: "", code: true };
        }
    }
    if ($("#HdnMode").val() == "Existing") {
        if ($("#CorrespondenceMessageFrom").val() == '') {
            $("#CorrespondenceMessageFrom").focus();
            return { msg: "Enter correspondence message from", code: false };
        }
        if ($("#CorrespondenceMessageFrom").val() != '') {
            var fromAddress = $("#CorrespondenceMessageFrom").val();
            if (!emailReg.test(fromAddress)) {
                $("#CorrespondenceMessageFrom").focus();
                return { msg: "Enter valid email address in correspondence message from", code: false };
            }
        }
        if ($("#LKCorrespondenceMessageFromRoleID").val() == '') {
            $("#LKCorrespondenceMessageFromRoleID").focus();
            return { msg: "Select role of the person", code: false };
        }
        if ($("#LKCorrespondenceMessageTypeID").val() == '') {
            $("#LKCorrespondenceMessageTypeID").focus();
            return { msg: "Select correspondence type", code: false };
        }
        if ($("#DAAIApprovalRequired").val() == 'N/A') {
            $("#DAAIApprovalRequired").focus();
            return { msg: "Select DAAI approval required", code: false };
        }
        if ($("#CorrespondenceContent").val() == '') {
            $("#CorrespondenceContent").focus();
            return { msg: "Enter correspondence content", code: false };
        }
        else {
            return { msg: "", code: true };
        }
    }
    if ($("#HdnMode").val() == "response") {
        if ($("#CorrespondenceContent").val() == '') {
            $("#CorrespondenceContent").focus();
            return { msg: "Enter correspondence content", code: false };
        }
        else {
            return { msg: "", code: true };
        }
    }

    //else if ($('input[name=DutyInvestigatorSuggestions]:checked').length <= 0) {
    //    TabIndex = 6;
    //    return { msg: "Select Duty Investigator Suggestions", code: false };
    //}
    //else if ($('input[name=DutyInvestigatorFurtherSuggestion]:checked').length <= 0) {
    //    TabIndex = 6;
    //    return { msg: "Select Duty Investigator Further Suggestion", code: false };
    //}

}
$("#btnaddCorres").click(function () {
    $("#AddCorres").show();
    $("#Existing_Corres").hide();
});

$("#crsrespnse").on("dp.change", function (e) {
    
    var fullDate = new Date();
    var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
    var currentdate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
    var todate = changeDateOnlyFormat($("#CorrespondenceDueDate").val());
    var fromdate = changeDateOnlyFormat(currentdate);
            var f = new Date(fromdate);
            var t = new Date(todate);
            var timeDiff = Math.abs(t.getTime() - f.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            $("#Corresrespnsdays").val(diffDays);
            $("#Corresrespnsdays").text(diffDays);
});

$("#Corresrespnsdays").on('input', function () {
    if ($("#Corresrespnsdays").val() != '') {
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
        var currentdate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var fromdate = changeDateOnlyFormat(currentdate);
        var f = new Date(fromdate);
        f.setDate(f.getDate() + parseInt($("#Corresrespnsdays").val()));
        //var twoDigitDay = ((f.getDate() <= 9) ? (f.getDate()) : '0' + (f.getDate());
        if (f.getDate() > 9) {
            var twoDigitDay = f.getDate();
        }
        else {
            var twoDigitDay = '0' + (f.getDate());
        }
        if (f.getMonth() > 8) {
            var twoDigitMonth1 = (f.getMonth() + 1)
        }
        else {
            var twoDigitMonth1 = '0' + (f.getMonth() + 1);
        }
       
        //var twoDigitMonth1 = ((f.getMonth().length + 1) === 1) ? (f.getMonth() + 1) : '0' + (f.getMonth() + 1);
        var currentdate = twoDigitDay + "/" + twoDigitMonth1 + "/" + f.getFullYear();
        $("#CorrespondenceDueDate").val(currentdate);
        $("#CorrespondenceDueDate").text(currentdate);
    }
});
$("#LKCorrespondenceMessageTypeID").change(function () {

    if ($("#LKCorrespondenceMessageTypeID").val() == "6") {

        $("#Corresrespnsdays").val(90);
        $("#Corresrespnsdays").text(90);
        if ($("#Corresrespnsdays").val() != '') {
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
            var currentdate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            var fromdate = changeDateOnlyFormat(currentdate);
            var f = new Date(fromdate);
            f.setDate(f.getDate() + parseInt($("#Corresrespnsdays").val()));
            //var twoDigitDay = ((f.getDate() <= 9) ? (f.getDate()) : '0' + (f.getDate());
            if (f.getDate() > 9) {
                var twoDigitDay = f.getDate();
            }
            else {
                var twoDigitDay = '0' + (f.getDate());
            }
            if (f.getMonth() > 8) {
                var twoDigitMonth1 = (f.getMonth() + 1)
            }
            else {
                var twoDigitMonth1 = '0' + (f.getMonth() + 1);
            }

            //var twoDigitMonth1 = ((f.getMonth().length + 1) === 1) ? (f.getMonth() + 1) : '0' + (f.getMonth() + 1);
            var currentdate = twoDigitDay + "/" + twoDigitMonth1 + "/" + f.getFullYear();
            $("#CorrespondenceDueDate").val(currentdate);
            $("#CorrespondenceDueDate").text(currentdate);
        }
    }
    else {
        $("#Corresrespnsdays").val(0);
        $("#Corresrespnsdays").text(0);
        $("#CorrespondenceDueDate").val("");
        $("#CorrespondenceDueDate").text("");
    }
    
});

$("#LKCorrespondenceMessageFromRoleID").change(function () {
    if ($("#LKCorrespondenceMessageFromRoleID").val() == "7") {
        $("#otherRoles").show()
    }
    else {
        $("#otherRoles").hide()
    }
});
$("#LKCorrespondenceMessageTypeID").change(function () {
    if ($("#LKCorrespondenceMessageTypeID").val() == "7") {
        $("#otherTypes").show()
        $("#SRNum").hide()
    }
    else if ($("#LKCorrespondenceMessageTypeID").val() == "6") {
        var invid = $("#InvestigationFileID").val();
        $.ajax ({
            url: '/IIC/searchSRs',
            method: 'POST',
            datatype: 'application/json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                InvId: invid,
            }),
            success: function (data) {
                var corresTags = JSON.parse(data);
                if (corresTags == "") {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>No SR have been added for this investigation</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                    $("#SRNum").hide();
                    $("#LKCorrespondenceMessageTypeID").val('');
                    $("#Corresrespnsdays").val(0);
                    $('#CorrespondenceDueDate').val('');
                    $("#Corresrespnsdays").text(0);
                    $("#otherTypes").hide()
                }
                else {
                    $("#SRNum").show()
                    $("#otherTypes").hide()
                }
            }
        });
    }
    else {
        $("#SRNum").hide()
        $("#otherTypes").hide()
    }
});
function ddlTypeNameChange() {

    $("#corresForm").submit();

}
function saveCorrespondence(fileElementId) {
    data = validateForm();
    if (data.code == true) {
        if ($("#HdnMode").val() == "New") {
            var initialdate = changeDateOnlyFormat($("#CorrespondenceInitiatedDate").val());
            $("#CorrespondenceInitiatedDate").val(initialdate);
            var duedate = changeDateOnlyFormat($("#CorrespondenceDueDate").val());
            $("#CorrespondenceDueDate").val(duedate);
            //var controlNum = ($("#CorrespondenceControlNumber").text());
            //$("#CorrespondenceControlNumber").text(controlNum);
        }
        if ($("#HdnMode").val() == "Existing") {
            var CID = $("#CorresID").val();
            $("#CorrespondenceID").val(CID);
            //var udto = changeDateFormat(new Date());
        }
        var count = $('.' + fileElementId).length;
        var file_Info = new Array(count);
        var formdata = new FormData();
        var formReceipt = new FormData();

        //if ($('.' + fileElementId)[0].files.length > 0) {
        //    for (var i = 0; i < count; ++i) {
        //        file_Info[i] = new Array(50);
        //        var inp = $('.' + fileElementId)[i];
        //        //var hdn = $('.' + hdnVal)[i];
        //        for (var j = 0; j < inp.files.length; j++) {
        //            file_Info[i][0] = inp.files[j].name;
        //            var f = inp.files[j];
        //            formdata.append(inp.files[j].name, inp.files[j]);
        //        }
        //    }
        //}
        if ($("#HdnMode").val() == "New") {
            var reccount = 0;

            if ($('#CorrespondenceMessageRecipientEmailAddress').val() != null && $('#CorrespondenceMessageRecipientEmailAddress').val() != "") {
                if ($('#CorrespondenceMessageRecipientEmailAddress').val().indexOf(',') > -1) {
                    reccount = $('#CorrespondenceMessageRecipientEmailAddress').val().split(',').length;
                    for (var jj = 0; jj < reccount; jj++) {
                        var recedetail = $("#CorrespondenceMessageRecipientEmailAddress").val().split(',')[jj].trim();
                        if (recedetail != null && recedetail != "" && recedetail != undefined) {
                            formReceipt.append("UName", recedetail);
                            formReceipt.append("mailType", "To");
                        }
                    }
                }
                //since in case of correspondence type is Safety Recommendations,only one To user can be added.
                else {
                    reccount = 1;
                    var recedetail = $("#CorrespondenceMessageRecipientEmailAddress").val().trim();
                    if (recedetail != null && recedetail != "" && recedetail != undefined) {
                        formReceipt.append("UName", recedetail);
                        formReceipt.append("mailType", "To");
                    }
                }
                if ($('#CorrespondenceMessageExtRecipientEmailAddress').val() != null && $('#CorrespondenceMessageExtRecipientEmailAddress').val() != "") {
                    var extRecCount = $("#CorrespondenceMessageExtRecipientEmailAddress").val().split(',').length;
                    for (var ii = 0; ii < extRecCount; ii++) {
                        var extrecedetail = $("#CorrespondenceMessageExtRecipientEmailAddress").val().split(',')[ii].trim();
                        if (extrecedetail != null && extrecedetail != "" && extrecedetail != undefined) {
                            formReceipt.append("UName", extrecedetail);
                            formReceipt.append("mailType", "CC");
                        }
                    }
                }
            }
        }
        $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
        $('.alert-info').show();
        $("#btnSaveCorres").hide();
        if ($("#HdnMode").val() == "New") {
            if (formReceipt != null) {
                var xhr1 = new XMLHttpRequest();
                xhr1.open('POST', '/IIC/saveRecipients?recipient=' + $("#CorrespondenceControlNumber").val());
                xhr1.send(formReceipt);
            }
            setTimeout(function () {
                $("#new_corrspondence").submit();
            }, 3000);
        }
        if ($("#HdnMode").val() == "response") {
            $("#new_corrspondence").submit();
        }
    }
    else {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>' + data.msg + '</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
}
var i = 1;
var fileupload = true;
$("#corres_add_attachment").click(function (e) {
    var controlcount = $('.corresupload').length;

    for (var ii = 0; ii < controlcount; ii++) {
        var inp = $('.corresupload')[ii];
        if (inp.files.length == 0) {
            fileupload = false;
            break;
        }
        else
            fileupload = true;
    }
    if (fileupload == false) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
    else {
        var html = '<div class="col-sm-12 col-form-box no-padding" style="margin-bottom: 15px;"><div class="input-group" ><input id="corresuploadFile_' + i + '" class="form-control" placeholder= "Choose File" disabled= "disabled" > <div class="input-group-btn"><div id="FileUpload_' + i + '" class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="corresuploadBtn_' + i + '" type="file" class="upload corresupload" name="CorresFiles[]" required="required"><input type="hidden" id="hdnVal_' + i + '" class="hdnfile"/></div><a href="javascript:void(0);" class="remove_Attached_file" onclick=removeCorresFiles(this) id="RemoveAttach_' + i +'"><i class="fa fa-times" aria-hidden="true"></i></a></div></div></div >';        
        //<div><a id="RemoveAttach_' + i + '" class="remove_CorresFileattachment" onclick=removeCorresFiles(this)>Remove</a></div>
        $(".attachments-tag").append(html);
        i++;
    }
});

$("#large_add_attachment").click(function (e) {
    var controlcount = $('.corresupload').length;

    
    if (controlcount > 2) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle">You are resctricted to upload only three files at a time</i></div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 5000);
        return false;
    }
    for (var ii = 0; ii < controlcount; ii++) {
        var inp = $('.corresupload')[ii];
        if (inp.files.length == 0) {
            fileupload = false;
            break;
        }
        else
            fileupload = true;
    }
    if (fileupload == false) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please choose a file</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
    }
    else {
        var html = '<div class="col-sm-12 col-form-box no-padding" id="largefilemaindiv_'+i+'" style="margin-bottom: 15px;"><div class="input-group" ><input id="corresuploadFile_' + i + '" class="form-control" placeholder= "Choose File" disabled= "disabled" > <div class="input-group-btn"><div id="FileUpload_' + i + '" class="fileUpload btn btn-success"><span><i class="fa fa-upload" aria-hidden="true"></i>Choose File</span><input id="corresuploadBtn_' + i + '" type="file" class="upload corresupload" name="CorresFiles[]" required="required"><input type="hidden" id="hdnVal_' + i + '" class="hdnfile"/></div><a href="javascript:void(0);" class="remove_Attached_file" onclick=removelargefilesFiles(this) id="RemoveAttach_' + i + '"><i class="fa fa-times" aria-hidden="true"></i></a></div></div></div >';
        //<div><a id="RemoveAttach_' + i + '" class="remove_CorresFileattachment" onclick=removeCorresFiles(this)>Remove</a></div>
        $(".attachments-tag").append(html);
        i++;
    }
});


function removelargefilesFiles(e) {
    var attach_ids = $(e).attr('id').split('_');
    var attach_name = $('#corresuploadFile_' + parseInt(attach_ids[1])).val();
    $('#corresuploadFile_' + parseInt(attach_ids[1])).remove();
    $('#corresuploadBtn_' + parseInt(attach_ids[1])).remove();
    $('#RemoveAttach_' + parseInt(attach_ids[1])).remove();
    $('#FileUpload_' + parseInt(attach_ids[1])).remove();
    $('#largefilemaindiv_' + parseInt(attach_ids[1])).remove();
    //var attachurl = $(this).prev().attr('href');

}
function removeCorresFiles(e) {
    var attach_ids = $(e).attr('id').split('_');
    var attach_name = $('#corresuploadFile_' + parseInt(attach_ids[1])).val();
    //var attachurl = $(this).prev().attr('href');
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/IIC/RemoveCorresAttachedFile',
        dataType: 'json',
        data: { filename: (attach_name) },
        error: function (err) {

        },
        success: function (result) {
            $('#corresuploadFile_' + parseInt(attach_ids[1])).remove();
            $('#corresuploadBtn_' + parseInt(attach_ids[1])).remove();
            $('#RemoveAttach_' + parseInt(attach_ids[1])).remove();
            $('#FileUpload_' + parseInt(attach_ids[1])).remove();
        }
    });
}

$(document.body).on('change', '.corresupload', function (e) {
    var img = $(this).val();
    var num = $(this).attr('id').split('_');
    var file_number = num[1];
    $("#corresuploadFile_" + file_number).val(img);



    var files = e.target.files;

    if (files.length > 0) {
        if (window.FormData !== undefined) {
            var data = new FormData();
            for (var x = 0; x < files.length; x++) {
                data.append("file" + x, files[x]);
            }

            $.ajax({
                type: "POST",
                url: '/IIC/UploadCorresFile',
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    console.log(result);
                },
                error: function (xhr, status, p3, p4) {
                    var err = "Error " + " " + status + " " + p3 + " " + p4;
                    if (xhr.responseText && xhr.responseText[0] == "{")
                        err = JSON.parse(xhr.responseText).Message;
                    console.log(err);
                }
            });
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }


});


function TestSPUpload(fileElementId) {
    var count = $('.' + fileElementId).length;
    var file_Info = new Array(count);
    var formdata = new FormData();
    if ($('.' + fileElementId)[0].files.length > 0) {
        for (var i = 0; i < count; ++i) {
            file_Info[i] = new Array(50);
            var inp = $('.' + fileElementId)[i];
            //var hdn = $('.' + hdnVal)[i];
            for (var j = 0; j < inp.files.length; j++) {
                file_Info[i][0] = inp.files[j].name;
                var f = inp.files[j];
                formdata.append(inp.files[j].name, inp.files[j]);
            }
        }
    }
    if (formdata != null) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/IIC/SaveFiles?control=' + "001");
        xhr.send(formdata);
    }
}
